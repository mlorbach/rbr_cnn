# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-10-27 10:20:57
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-10-27 10:23:36
import os
import errno
import numpy as np
import pandas as pd
import json
import argparse


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def read_annotations(filename, continuous=False):
    '''
    Read annotations from original CRIM13 annotation file.

    Parameters
    ----------

    filename : str
        Path to file to read

    continuous : bool
        If True, convert (start, end)-like events to continuous frame annotations (1,2,3, ...)

    Returns
    -------

    pd.DataFrame
        Annotations with frame index and action column. Has column `frame_stop` if continuous=False.
    '''

    # Load RAW CRIM annotation files
    dfa = pd.read_csv(filename, skiprows=19, header=None,
                      delim_whitespace=True, index_col=0,
                      names=['frame', 'frame_stop', 'action'])

    # drop state point event and empty events
    dfa = dfa.loc[(dfa['action'] != ''), :]

    if continuous:
        first = dfa.index.min()
        last = dfa['frame_stop'].iloc[-1]
        dfa = dfa.drop('frame_stop', axis=1)
        dfa = dfa.reindex(np.arange(first, last + 1)).fillna(method='ffill')

    return dfa


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('annotation_files', type=str, nargs='+',
                        help='Input .txt-files with annotations in original CRIM13 format')
    parser.add_argument('--offsets', type=str,
                        help='Path to json-file containing the frame offsets used when extracting video frames.')
    parser.add_argument('output_path', type=str,
                        help='Path to output files.')
    args = parser.parse_args()

    N_videos = len(args.annotation_files)
    print 'Scheduling to extract annoations from {} videos.'.format(N_videos)
    print 'Output path: {}'.format(args.output_path)
    mkdir_p(args.output_path)

    # read frame offsets
    if args.offsets is not None:
        if os.path.isfile(args.offsets):
            with open(args.offsets, 'r') as f_offset:
                offset_dict = json.load(f_offset)
        else:
            raise ValueError('Could not read offset file: {}'.format(args.offsets))
    else:
        offset_dict = {}

    for i_vid, annfile in enumerate(args.annotation_files):

        vidname = os.path.splitext(os.path.basename(annfile))[0]
        print '[{:3d}/{:3d}] Converting {}...'.format(i_vid + 1,
                                                      N_videos,
                                                      vidname)

        # read annotations
        dfa = read_annotations(annfile, continuous=True)

        # clip given offset and reindex
        if vidname in offset_dict:
            start_frame = offset_dict[vidname]['start_frame']
            end_frame = offset_dict[vidname]['end_frame']

            print '  [INFO] Clip to range [{}:{}]'.format(start_frame, end_frame)

            dfa = dfa.loc[start_frame:end_frame, :]

            dfa.index -= start_frame - 1
            if 'frame_stop' in dfa.columns:
                dfa['frame_stop'] -= start_frame - 1
        else:
            print '  [WARN] {} not in offsets'.format(vidname)

        # write to csv
        out = os.path.join(args.output_path, vidname + '.csv')
        dfa.to_csv(out, sep=';')

    print 'Done.'
