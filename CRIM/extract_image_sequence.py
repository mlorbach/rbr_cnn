# -*- coding: utf-8 -*-
# @Author: Malte Lorbach
# @Date:   2017-10-27 10:20:57
# @Last Modified by:   Malte Lorbach
# @Last Modified time: 2017-10-27 10:23:40
import os
import errno
import numpy as np
import json
from struct import unpack, unpack_from
from PIL import Image
from cStringIO import StringIO
import argparse
from matlab import load_matlab_tracks
import cv2


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def extract_video(seq_file, output_path, start_frame=0, end_frame=None,
                  file_pattern='frame_{:08d}.jpg',
                  resize=(0, 0)):

    video_name = os.path.splitext(os.path.basename(seqfile))[0]
    is_video_out = os.path.splitext(file_pattern)[1] in ['.mp4', '.avi']
    is_resize = (resize[0] > 0) and (resize[1] > 0)

    offset_bytes = 36 + 512
    length_size = 8

    with open(seq_file, mode='r') as f:

        # read meta data from seq file
        pack = f.read(offset_bytes + length_size)
        size = unpack('<LL', pack[-length_size:])
        bpp, _, _, _, nFrames, _, _ = unpack('<7L', f.read(7*4))
        fps_sugg = unpack('<d', f.read(8))[0]

        untilnow = offset_bytes + length_size + 7*4 + 8

        if not is_resize:
            resize = size

        print '  Image size (in):  {}'.format(size)
        print '  Image size (out): {}'.format(resize)
        print '  FPS: {}'.format(fps_sugg)

        # go to data:
        f.read(1024 - untilnow)

        imgdtype = np.dtype('uint%i' % (bpp))
        databytes = unpack('<L', f.read(4))[0] - 4

        if is_video_out:
            # setup video writer
            codec = cv2.VideoWriter_fourcc(*'X264')
            file_name = os.path.join(output_path, file_pattern.format(video_name))
            vidwriter = cv2.VideoWriter(file_name, codec, float(fps_sugg), resize, False)

        global_i = 0
        # skip frames until start_frame
        for i in range(start_frame):
            try:
                f.read(databytes+16)  # don't need it
                databytes = unpack('<L', f.read(4))[0]-4
                global_i += 1
            except Exception as e:
                print '[ERROR] Exception skipping frames:', e
                break

        for i in range(end_frame - start_frame + 1):

            imgdata = np.asarray(unpack('{:d}B'.format(databytes), f.read(databytes)), dtype=imgdtype)
            img = Image.open(StringIO(imgdata))

            if is_resize:
                # img = cv2.resize(img, resize, interpolation=cv2.INTER_CUBIC)
                img = img.resize(resize, Image.BICUBIC)

            if is_video_out:
                vidwriter.write(np.array(img))
            else:
                img.save(os.path.join(output_path, file_pattern.format(i + 1)))

            # time stamp and validation flag
            ts, valid = unpack('<2q', f.read(16))
            if valid != 0:
                print '[ERROR] Corrupt frame. Break.'
                break
            try:
                # get byte length of next frame
                databytes = unpack('<L', f.read(4))[0] - 4
            except Exception as e:
                # this is not relevant if we reached the end of the video
                if global_i < nFrames:
                    print '[ERROR] Exception reading images:', e
                    break
            global_i += 1

        if is_video_out:
            vidwriter.release()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('seqfiles', type=str, nargs='+',
                        help='Input .seq-files, multiple arguments or listed in .txt file')
    parser.add_argument('output_path', type=str,
                        help='Path to output images; a subfolder is created for each input file.')
    parser.add_argument('--resize', '-r', type=int, default=0,
                        help='Resize image to square of the given pixel size.')
    parser.add_argument('--format', '-f', type=str, choices=['jpg', 'png', 'mp4', 'avi'],
                        default='jpg', help='Image/Video format')
    args = parser.parse_args()

    if len(args.seqfiles) == 1 and args.seqfiles[0].endswith('.txt'):
        txtfile = args.seqfiles[0]
        seqfiles = []
        with open(txtfile, 'rb') as fs:
            seqfiles = fs.read().splitlines()
    else:
        seqfiles = args.seqfiles

    N_videos = len(seqfiles)
    print 'Scheduling to convert {} videos.'.format(N_videos)
    print 'Output path: {}'.format(args.output_path)

    # we store the frame offsets that we apply in a json file.
    # Append it if it exists:
    offsets_filename = os.path.join(args.output_path, 'offsets.json')
    if os.path.isfile(offsets_filename):
        with open(offsets_filename, 'r') as f_offset:
            offset_dict = json.load(f_offset)
        print 'Appending to existing frame offsets file.'
    else:
        offset_dict = {}
        print 'Creating new frame offsets file.'

    for i_vid, seqfile in enumerate(seqfiles):

        dft = load_matlab_tracks(os.path.splitext(seqfile)[0] + '-track.mat').dropna(axis=0)

        start_frame = dft.first_valid_index()
        end_frame = dft.last_valid_index()

        if args.format in ['mp4', 'avi']:
            out = args.output_path
            fmt = '{}.' + args.format
        else:
            out = os.path.join(args.output_path, os.path.splitext(os.path.basename(seqfile))[0])
            fmt = 'frame_{:08d}.' + args.format
        mkdir_p(out)

        print '[{:3d}/{:3d}] Video: {}; frames [{:d}:{:d}]'.format(i_vid + 1,
                                                                   N_videos,
                                                                   os.path.basename(seqfile),
                                                                   start_frame,
                                                                   end_frame)

        offset_dict[os.path.splitext(os.path.basename(seqfile))[0]] = {
            'start_frame': start_frame,
            'end_frame': end_frame
        }
        extract_video(seqfile, out, start_frame, end_frame, file_pattern=fmt,
                      resize=(args.resize, args.resize))

    with open(offsets_filename, 'w') as f_offset:
        json.dump(offset_dict, f_offset, indent=2)

    print 'Done.'
