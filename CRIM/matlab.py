# -*- coding: utf-8 -*-
"""
Created on Thu Apr 02 14:23:25 2015

@author: MAL
"""

import pandas as pd
import numpy as np
import logging
import os

from scipy.io import loadmat


def load_matlab_tracks(matfile, stacked_columns=False):
    '''
    Loads original MATLAB track file from CRIM13 data set.

    Positions are not interpolated or filled otherwise. I.e., missing information is stored as np.nan.

    Parameters
    ----------
    matfile : str
        File to load

    Returns
    -------
    dftracks : pd.DataFrame
        [N x 2*n_subjects] DataFrame with MultiIndexed columns. 1st level is subject, 2nd level is X, Y.

        Index is frame-based starting with 0.
    '''

    logger = logging.getLogger(__name__)

    if not os.path.isfile(matfile):
        raise ValueError('The specified file does not exist: "{}".'.format(matfile))

    # load matlab file
    data = loadmat(matfile, chars_as_strings=True, squeeze_me=True)

    ###############################################################
    # convert the weird array of arrays format into a Nx4 matrix:

    # allocate matrix
    a = np.empty( (len(data['Y']), 4) )
    a.fill(np.nan)

    # find for each each frame, the number of animals tracked (1 vs 2 / 2 vs 4 pts)
    sz = np.asarray([d.size for d in data['Y'].tolist()])

    # write all 2-pt frames and 4-pt frames into the matrix
    a[ sz==2, 0:2] = np.vstack(data['Y'][ sz == 2 ])
    a[ sz==4, :] = np.reshape(np.dstack( data['Y'][ sz == 4 ] ), (4, np.sum(sz == 4))).T

    ###############################################################
    # Create Tracks DataFrame

    dftracks = pd.DataFrame(a)

    if stacked_columns:
        dftracks.columns = pd.MultiIndex.from_product( [ ('subject_1', 'subject_2'), ('center'), ('X', 'Y')] )
        dftracks.columns.names = ['subject', 'name', 'feature']
    else:
        dftracks.columns = pd.MultiIndex.from_product( [ ('subject_1', 'subject_2'), ('X center', 'Y center')] )
        dftracks.columns.names = ['subject', 'feature']

    dftracks.index.name = 'frame'

    return dftracks
