CNN-based Rodent Behavior Recognition
=====================================

Cloning
-------

Make sure to clone recursively to receive all external submodules.

```sh
git clone --recursive git@gitlab.com:mlorbach/rbr_cnn.git .
```


Rodent Tracking
---------------

We made a little progress on CNN based rodent tracking and social behavior recognition. It's all very experimental though. See [rtracker_caffe](rtracker_caffe) for more details and [notes/rtracker_results](notes/rtracker_results) for preliminary results.