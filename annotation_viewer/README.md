### Annotation Viewer

Display video with overlayed action/behavior annotations.

- Shows current playback time
- Supports displaying two labels simulataneously (e.g. ground truth + prediction)
- Pause (SPACE) and stop (ESC) functions
- Issues warning if video and labels are not in sync anymore
- Option to export video with overlays


Label file format:

First row contains the names of the label columns (e.g. "GT;Prediction"). Columns are separated by ';'.

```
column 1;column2
label1_frame1;label2_frame_1
label1_frame2;label2_frame_2
label1_frame3;label2_frame_3
...
```
