/*
* @Author: Malte Lorbach
* @Date:   2017-10-27 10:20:57
* @Last Modified by:   Malte Lorbach
* @Last Modified time: 2017-10-27 10:24:38
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace pt = boost::posix_time;


void drawFrameBackground(cv::Mat& image, cv::Scalar color, const float alpha = 0.4, const float frame_height_frac = 0.1f) {

	cv::Rect frame(cv::Point(0, image.rows), cv::Point(image.cols, int(round(image.rows * (1 - frame_height_frac)))));
    cv::Mat overlay = image.clone();
    cv::rectangle(overlay, frame, color, CV_FILLED);
    cv::addWeighted(overlay, alpha, image, 1.-alpha, 0, image);
}

void drawFrameBackground(cv::Mat& image, const float alpha = 0.4, const float frame_height_frac = 0.1) {
	drawFrameBackground(image, cv::Scalar(0,0,0), alpha, frame_height_frac);
}

void drawLabels(cv::Mat& image, const std::string label, const std::string pred = "", const float frame_height_frac = 0.1f) {

    int baseline=0;
    const int fontFace = cv::FONT_HERSHEY_TRIPLEX;
    const double fontScale = .6;
    const int thickness = 1;

    std::string disp_label("GT: " + label);

    cv::Size labelSize = cv::getTextSize(disp_label, fontFace, fontScale, thickness, &baseline);
    cv::Size predSize = cv::getTextSize(pred, fontFace, fontScale, thickness, &baseline);

    const float y_pos = int(round(image.rows * (1 - frame_height_frac/2.) + labelSize.height / 2.0f));

    const cv::Point labelPos(int(round(image.cols / 4.0f - labelSize.width / 2.0f)), y_pos);
    const cv::Point predPos(int(round(3 * image.cols / 4.0f - predSize.width / 2.0f)), y_pos);

    cv::Scalar pred_color(20,20,220);
    if(label == pred)
    	pred_color = cv::Scalar(20,220,20);

    cv::putText(image, disp_label, labelPos, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);
    cv::putText(image, pred, predPos, fontFace, fontScale, pred_color, thickness, 8);
}

void drawTime(cv::Mat& image, const float msec) {
	int baseline=0;
	const int fontFace = cv::FONT_HERSHEY_PLAIN;
	const double fontScale = .8;
	const int thickness = 1;

	pt::time_duration td(0,0,0);
	td += pt::milliseconds(msec);
	std::stringstream timestr;

	pt::time_facet* facet = new pt::time_facet();
	facet->format("%H:%M:%S.%f");
	timestr.imbue(std::locale(std::locale::classic(), facet));
	timestr << td;

	//timestr << td.hours() << ":" << td.minutes() << ":" << td.seconds() << ":" << td.fractional_seconds();

	cv::putText(image, timestr.str().substr(0, 12), cv::Point(20, 20), fontFace, fontScale, cv::Scalar::all(255), thickness, 8);
}

std::vector<std::string> getColumnNames(const std::string& line, const char sep = ';') {
	std::stringstream temp(line);
	std::string column;
	std::vector<std::string> ret;

	while(getline(temp, column, sep)){
		ret.push_back(column);
	}

	return ret;
}

std::pair<int, std::vector<std::string> > parseLabels(const std::string& line, const char sep = ';') {
	int id;
	std::stringstream temp(line);
	std::string column;
	std::vector<std::string> labels;

	for(size_t i = 0; getline(temp, column, ';'); ++i){
		if(i==0)
			id = atoi(column.c_str());
		else
			labels.push_back(column);
	}

	return std::make_pair(id, labels);
}

int main(int argc, const char *argv[])
{
	fs::path video_path, labels_path, output_path;
	bool store_video;

	/***************************************/
	// COMMAND LINE ARGUMENTS
	/***************************************/

	// Declare the supported options.
	po::options_description desc("Usage");
	desc.add_options()
		("help,h", "produce help message")
		("video", po::value<fs::path>(&video_path)->required(), "Path to input video")
		("labels", po::value<fs::path>(&labels_path)->required(), "Path to labels file")
		("output_path,o", po::value<fs::path>(&output_path),
				"Output path for saving video (optional).")
		("store,s", po::bool_switch(&store_video), "Save video to output path instead of displaying it.")
	;

	po::positional_options_description positionalOptions;
	    positionalOptions.add("video", 1);
	    positionalOptions.add("labels", 1);

	po::variables_map vm;

	try {

		po::store(po::command_line_parser(argc, argv).options(desc).positional(positionalOptions).run(), vm);

		if (vm.count("help")) {
			std::cout << desc << std::endl;
			return 1;
		}

		po::notify(vm);
	}
	catch(po::error& e) {
		std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
		std::cerr << desc << std::endl;
		return 1;
	}

	// --------------------------------------
	//  Open annotations
	// --------------------------------------

	std::ifstream fh_labels(labels_path.c_str());
	if (!fh_labels.is_open())
	{
		std::cerr << "[ERROR] Failed to open annotations file: " << labels_path << std::endl;
		return 1;
	}

	// retrieve column names (header)
	std::string line;
	getline(fh_labels, line);  // first line
	std::vector<std::string> column_names = getColumnNames(line);

	// --------------------------------------
	//  Open Video
	// --------------------------------------

	cv::Mat frame;
    namedWindow("Video", cv::WINDOW_AUTOSIZE);

    cv::VideoCapture cap(video_path.string());
    int wait_ms = 1000./cap.get(cv::CAP_PROP_FPS);
    char k;
    bool cancel = false;
    bool paused = false;

    size_t frame_count = 0;
    pt::ptime start_time;
    int elapsed;
    while ( cap.isOpened() && !cancel )
    {
    	start_time = pt::microsec_clock::local_time();
    	if(!paused) {
    		frame_count++;

    		// Image
			cap >> frame;
			if(frame.empty()) {
				std::cout << "Reached last frame." << std::endl;
				break;
			}

			drawFrameBackground(frame);

			// Labels
			getline(fh_labels, line);
			std::pair<int, std::vector<std::string> > labels = parseLabels(line);

			if(labels.second.size() >= 2)
				drawLabels(frame, labels.second.at(1), labels.second.at(0));

			drawTime(frame, cap.get(cv::CAP_PROP_POS_MSEC));

			cv::imshow("Video", frame);

			if(frame_count % 250 == 0) {
				if(cap.get(cv::CAP_PROP_POS_FRAMES) != labels.first || labels.first != frame_count) {
					std::cerr << "Synchronization failing: " << "Video: " << cap.get(cv::CAP_PROP_POS_FRAMES)  <<
							", Labels: " << labels.first << ", Count: " << frame_count << std::endl;
				}
			}
    	}

    	// incorporate processing time in playback speed:
    	elapsed = (pt::microsec_clock::local_time() - start_time).total_milliseconds();
    	k = cv::waitKey(std::max(1, wait_ms - elapsed));
		switch(k)
		{
		case 'q':
		case 27:  // ESC
			cancel = true;
			break;
		case ' ': // SPACE
			paused = !paused;
			break;
		}
    }

    fh_labels.close();

    return 0;
}
