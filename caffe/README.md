### Apply multi HDF5 output patch ###

https://github.com/BVLC/caffe/pull/5237

```sh
cp 5237_multiHDF5Output.patch <cafferoot>
cd <cafferoot>
git am 5237_multiHDF5Output.patch
# rebuild
cd build
cmake ..
make
```
