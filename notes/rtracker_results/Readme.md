RTracker Results (Behavior Classification)
==========================================

These are preliminary results obtained mostly to get acquanted with different architectures and what they do with rodent videos. Some networks were pre-trained on ImageNet and then fine-tuned on our data, others were trained from scratch from our data. Most of the time, some form of data augmentation was used: mirror and rotate in 90 deg steps.

All results obtained on [RatSI dataset](http://www.noldus.com/innovationworks/datasets/ratsi):

M. Lorbach, E. I. Kyriakou, R. Poppe, E. A. van Dam, L. P. J. J. Noldus, and R. C. Veltkamp, [Learning to Recognize Rat Social Behavior: Novel Dataset and Cross-Dataset Application](http://dx.doi.org/10.1016/j.jneumeth.2017.05.006), Journal of Neuroscience Methods, 2017.


Best performing networks
------------------------

All networks are trained with a weighted InfoGainLoss for better balancing minority classes.


### Baseline (tracking features only)

This is essentially just a linear classification model applied to 6 already extracted trajectory features (distances, velocity, change of distance/velocity).

- Network: 6 features -> one FC layer with 5 outputs
- Input: Tracking features (dist, v0, v1, dist_dt, v0_dt, v1_dt)
  - For comparison with video networks, applied to averages over 12|1 (stride 3) volumes
- Output: 5-class
- Path: `/home/malte/Documents/Results/2017-03-09_Baseline_track_feats_only`

```
             precision    recall  f1-score   support

Approaching       0.43      0.59      0.50      1285
    Contact       0.60      0.92      0.73      3788
  Following       0.54      0.27      0.36       821
Moving away       0.21      0.40      0.27       596
   Solitary       0.95      0.67      0.79      8655

avg / total       0.77      0.69      0.71     15145
avg / class       0.55      0.57      0.53         5
```
![Confusion matrix](images/cm_tf-only.png)
![Training progress](images/tp_tf-only.png)


### Single image, appearance features

- Network: VGG-M 2048
- Input: single images, RGB
- Output: 5-class
- Training: Fixed conv1-4, train conv5 - fc8 from scratch
  - reduced layer sizes to match vidcaf_motion
    - reduced conv5 from 512 to 256
    - reduced fc6 from 4096 to 2048
- Path: `/home/malte/Documents/Results/2017-03-15_VGG-M/exp04`

```
             precision    recall  f1-score   support

Approaching       0.20      0.40      0.27      3787
    Contact       0.66      0.68      0.67     11328
  Following       0.26      0.33      0.29      2461
Moving away       0.09      0.20      0.13      1812
   Solitary       0.91      0.68      0.78     26069

avg / total       0.72      0.62      0.66     45457
avg / class       0.43      0.46      0.43         5
```

![Confusion matrix VGG-M](images/cm_vgg-m.png)
![Training progress VGG-M](images/tp_vgg-m.png)


### Image volumes, 3D Convolutions

- Network: C3D (pre-trained UCF); 5 conv + 3 fc
- Input: Clips 12|1 stride 3
- Output: 5-class
- Training: Finetune conv1-2, rest from scratch
- Path: `/home/malte/Documents/Results/2017-03-13_VidCaf_motion/exp12/blr_0.01__wd_5e-4`

```
             precision    recall  f1-score   support

Approaching       0.26      0.33      0.29      1285
    Contact       0.60      0.84      0.70      3788
  Following       0.35      0.30      0.32       821
Moving away       0.13      0.27      0.18       596
   Solitary       0.93      0.68      0.78      8655

avg / total       0.73      0.65      0.67     15145
avg / class       0.45      0.48      0.45         5
```

![Confusion matrix](images/cm_c3d.png)
![Training progress](images/tp_c3d.png)


### Image volumes, 3D Convolutions (reduced)

- Network: C3D (pre-trained UCF); 5 conv + 3 fc
  - slightly adapted architecture so that temporal dimension collapses after conv5
  - further reduced size to decrease number of parameters
- Input: Clips 12|1 stride 3
- Output: 5-class
- Training: Finetune conv1-2, rest from scratch
- Path: `/home/malte/Documents/Results/2017-03-13_VidCaf_motion/exp14/256`

```
             precision    recall  f1-score   support

Approaching       0.24      0.31      0.27      1285
    Contact       0.56      0.85      0.68      3788
  Following       0.41      0.28      0.33       821
Moving away       0.09      0.18      0.12       596
   Solitary       0.94      0.66      0.77      8655

avg / total       0.72      0.64      0.66     15145
avg / class       0.45      0.45      0.43         5
```

![Confusion matrix](images/cm_c3d-red.png)
![Training progress](images/tp_c3d-red.png)