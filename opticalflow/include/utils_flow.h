#ifndef UTILS_FLOW_H
#define UTILS_FLOW_H

#include <stdio.h>
#include <iostream>
#include <vector>
#include <numeric>
#include <deque>
#include <opencv2/core.hpp>
#include "opencv2/core/cuda.hpp"

void convertFlowToImage(const cv::Mat &flowIn, cv::Mat &flowOut, float bound);
void convertFlowToImage(const cv::cuda::GpuMat &flowIn, cv::cuda::GpuMat &flowOut, float bound);

void normalizeFlow(const cv::cuda::GpuMat& flow_u, const cv::cuda::GpuMat& flow_v,
		cv::cuda::GpuMat& norm_flow_u, cv::cuda::GpuMat& norm_flow_v,
		std::deque<cv::Point2f>& mean_vecs, cv::Point2f& principal_motion,
		const int norm_win_size = 9, const float norm_pixel_flow_thresh = 1.5f, const float norm_global_flow_thresh = 3.0f);

//inline void normalizeFlow(const cv::cuda::GpuMat& flow_u, const cv::cuda::GpuMat& flow_v,
//		cv::cuda::GpuMat& norm_flow_u, cv::cuda::GpuMat& norm_flow_v, std::deque<cv::Point2f>& mean_vecs)
//{
//	cv::Point2f avg_mean_flow;
//	normalizeFlow(flow_u, flow_v, norm_flow_u, norm_flow_v, mean_vecs, avg_mean_flow);
//}

cv::Vec3b computeColor(float fx, float fy);
void drawOpticalFlow(const cv::Mat_<float>& flowx, const cv::Mat_<float>& flowy, cv::Mat& dst, float maxmotion = -1);
void drawOpticalFlow(const cv::cuda::GpuMat& flowx, const cv::cuda::GpuMat& flowy, cv::Mat& dst, float maxmotion = -1);
cv::Mat get_histogram(const cv::Mat& img, bool uniform = true, bool accumulate = false);

#endif // UTILS_FLOW_H
