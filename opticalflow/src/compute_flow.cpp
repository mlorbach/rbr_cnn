/*
 * compute_flow.cpp
 *
 *  Created on: Feb 10, 2017
 *      Author: malte
 */


#include <iostream>
#include <fstream>

#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/core/cuda.hpp"
#include "opencv2/cudaoptflow.hpp"
#include "opencv2/cudaarithm.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"

#include "utils_flow.h"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

using namespace std;
using namespace cv;
using namespace cv::cuda;

namespace po = boost::program_options;
namespace fs = boost::filesystem;

// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
std::vector<fs::path> get_image_files(const fs::path& root, const string& ext = ".jpg")
{
	std::vector<fs::path> ret;

    if(!fs::exists(root) || !fs::is_directory(root))
    	return ret;

    fs::recursive_directory_iterator it(root);
    fs::recursive_directory_iterator endit;

    while(it != endit)
    {
        if(fs::is_regular_file(*it) && it->path().extension() == ext)
        	ret.push_back(it->path().filename());
        ++it;
    }

    std::sort(ret.begin(), ret.end());

    return ret;
}

string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

int main(int argc, const char* argv[])
{
	int limit_frames, skip_frames, alg_type, output_size;
	fs::path input_path, output_path;
	bool is_video_out, save_hsv, debug;
	float clip_value;
	std::string image_file_ext;

	int norm_win_size;
	float norm_pixel_flow_thresh, norm_global_flow_thresh;

	string config_file;

	// Brox parameters
	float para_brox_alpha,
		  para_brox_gamma,
		  para_brox_scale;
	int para_brox_inner_iter,
		para_brox_outer_iter,
		para_brox_solver_iter;

	// TVL1 parameters
	float para_tvl1_tau,
		  para_tvl1_lambda,
		  para_tvl1_theta,
		  para_tvl1_eps,
		  para_tvl1_scale_step,
		  para_tvl1_gamma;
	int para_tvl1_nscales,
		para_tvl1_warps,
		para_tvl1_iterations;
	bool para_tvl1_useInitialFlow;

	// farneback parameters
	int para_farn_numLevels,
		para_farn_winSize,
		para_farn_numIters,
		para_farn_polyN,
		para_farn_flags;
	float para_farn_pyrScale,
		  para_farn_polySigma;
	bool para_farn_fastPyramids;

	// dense LK parameters
	int para_lk_winSize,
		para_lk_maxLevel,
		para_lk_iters;
	bool para_lk_useInitialFlow;



	/***************************************/
	// COMMAND LINE ARGUMENTS
	/***************************************/

	// Declare the supported options.
	po::options_description generic("Usage");
	generic.add_options()
	    ("help,h", "produce help message")
	    ("input_path,I", po::value<fs::path>(&input_path)->default_value("/home/malte/datasets/RatSI/videos/Observation01.mp4"),
	    		"Path to input images")
	    ("output_path,O", po::value<fs::path>(&output_path)->default_value("output"),
	    		"Output path")
	    ("scale,s", po::value<int>(&output_size)->default_value(0), "Rescale output to NxN")
	    ("limit,l", po::value<int>(&limit_frames)->default_value(-1), "Number of images to process (-1: all)")
	    ("skip", po::value<int>(&skip_frames)->default_value(1), "Use every nth frame (default: 1)")
	    ("algorithm,a", po::value<int>(&alg_type)->default_value(0), "Flow algorithm (0: TVL1, 1: Brox, 2: Farneback, 3: Dense LK)")
	    ("clip", po::value<float>(&clip_value)->default_value(10), "Value at which to clip the flow [-clip_value, clip_value]. Set to 0 to disable.")
	    ("debug,d", po::bool_switch(&debug)->default_value(false), "Show debug output.")
	    ("config,c", po::value<string>(&config_file)->default_value("../config/default.cfg"), "Configuration file to optical flow parameters.")
	;

	// Declare a group of options that will be
	// allowed both on command line and in
	// config file
	po::options_description config("Configuration");
	config.add_options()

		("image_ext", po::value<std::string>(&image_file_ext)->default_value(".jpg"), "File extension of input images")
		("save_hsv", po::value<bool>(&save_hsv)->default_value(false), "Save HSV image")
		("norm_win_size", po::value<int>(&norm_win_size)->default_value(9), "Length of moving window for flow normalization")
		("norm_pixel_flow_thresh", po::value<float>(&norm_pixel_flow_thresh)->default_value(1.5), "Threshold for pixel flow")
		("norm_global_flow_thresh", po::value<float>(&norm_global_flow_thresh)->default_value(3.0), "Threshold for global flow magnitude")

		("brox_alpha", po::value<float>(&para_brox_alpha)->default_value(0.5), "Flow parameter alpha: flow smoothness")
		("brox_gamma", po::value<float>(&para_brox_gamma)->default_value(40.0), "Flow parameter gamma: gradient constancy importance")
		("brox_scale", po::value<float>(&para_brox_scale)->default_value(0.7), "Flow parameter scale: pyramid scale factor")
		("brox_inner_iter", po::value<int>(&para_brox_inner_iter)->default_value(20), "Flow parameter inner_iterations: lagged non-linearity iterations")
		("brox_outer_iter", po::value<int>(&para_brox_outer_iter)->default_value(5), "Flow parameter outer_iterations: warping iterations (number of pyramid levels)")
		("brox_solver_iter", po::value<int>(&para_brox_solver_iter)->default_value(5), "Flow parameter solver_iterations: linear solver solver iterations")

		("tvl1_tau", po::value<float>(&para_tvl1_tau)->default_value(0.25), "help")
		("tvl1_lambda", po::value<float>(&para_tvl1_lambda)->default_value(0.15), "help")
		("tvl1_theta", po::value<float>(&para_tvl1_theta)->default_value(0.3), "help")
		("tvl1_nscales", po::value<int>(&para_tvl1_nscales)->default_value(5), "help")
		("tvl1_warps", po::value<int>(&para_tvl1_warps)->default_value(5), "help")
		("tvl1_eps", po::value<float>(&para_tvl1_eps)->default_value(0.01), "help")
		("tvl1_iterations", po::value<int>(&para_tvl1_iterations)->default_value(300), "help")
		("tvl1_scale_step", po::value<float>(&para_tvl1_scale_step)->default_value(0.8), "help")
		("tvl1_gamma", po::value<float>(&para_tvl1_gamma)->default_value(0), "help")
		("tvl1_useInitialFlow", po::value<bool>(&para_tvl1_useInitialFlow)->default_value(false), "help")

		("farn_numLevels", po::value<int>(&para_farn_numLevels)->default_value(5), "help")
		("farn_pyrScale", po::value<float>(&para_farn_pyrScale)->default_value(0.5), "help")
		("farn_fastPyramids", po::value<bool>(&para_farn_fastPyramids)->default_value(false), "help")
		("farn_winSize", po::value<int>(&para_farn_winSize)->default_value(13), "help")
		("farn_numIters", po::value<int>(&para_farn_numIters)->default_value(10), "help")
		("farn_polyN", po::value<int>(&para_farn_polyN)->default_value(5), "help")
		("farn_polySigma", po::value<float>(&para_farn_polySigma)->default_value(1.1), "help")
		("farn_flags", po::value<int>(&para_farn_flags)->default_value(0), "help")

		("lk_winSize", po::value<int>(&para_lk_winSize)->default_value(13), "help")
		("lk_maxLevel", po::value<int>(&para_lk_maxLevel)->default_value(3), "help")
		("lk_iters", po::value<int>(&para_lk_iters)->default_value(30), "help")
		("lk_useInitialFlow", po::value<bool>(&para_lk_useInitialFlow)->default_value(false), "help")
	;

	po::options_description cmdline_options;
	cmdline_options.add(generic).add(config);

	po::options_description config_file_options;
	config_file_options.add(config);

	po::variables_map vm;
	store(po::command_line_parser(argc, argv).
		  options(cmdline_options).run(), vm);
	notify(vm);

	ifstream ifs(config_file.c_str());
	if (!ifs)
	{
		cout << "Cannot open config file: " << config_file << endl;
		return 1;
	}
	else
	{
		// read and parse config file
		store(parse_config_file(ifs, config_file_options), vm);
		notify(vm);
	}

	if (vm.count("help")) {
	    cout << generic << "\n";
	    return 1;
	}

	cout << "Using input path: " << vm["input_path"].as<fs::path>() << "\n";
	cout << "Number of images to process limited to " << limit_frames << ".\n";

	cout << "-----------------------" << endl;
	cout << "Optical flow parameters" << endl;
	cout << "-----------------------" << endl;
	cout << "  Type = " << alg_type << endl;

	switch(alg_type){
	case 0:
		cout << "  tau = " << para_tvl1_tau << endl;
		cout << "  lambda = " << para_tvl1_lambda << endl;
		cout << "  theta = " << para_tvl1_theta << endl;
		cout << "  nscales = " << para_tvl1_nscales << endl;
		cout << "  warps = " << para_tvl1_warps << endl;
		cout << "  eps = " << para_tvl1_eps << endl;
		cout << "  iterations = " << para_tvl1_iterations << endl;
		cout << "  scale_step = " << para_tvl1_scale_step << endl;
		cout << "  gamma = " << para_tvl1_gamma << endl;
		cout << "  useInitialFlow = " << para_tvl1_useInitialFlow << endl;
		break;
	case 1:
		cout << "  alpha = " << para_brox_alpha << endl;
		cout << "  gamma = " << para_brox_gamma << endl;
		cout << "  scale = " << para_brox_scale << endl;
		cout << "  inner_iter = " << para_brox_inner_iter << endl;
		cout << "  outer_iter = " << para_brox_outer_iter << endl;
		cout << "  solver_iter = " << para_brox_solver_iter << endl;
		break;
	case 2:
		cout << "  numLevels = " << para_farn_numLevels << endl;
		cout << "  pyrScale = " << para_farn_pyrScale << endl;
		cout << "  fastPyramids = " << para_farn_fastPyramids << endl;
		cout << "  winSize = " << para_farn_winSize << endl;
		cout << "  numIters = " << para_farn_numIters << endl;
		cout << "  polyN = " << para_farn_polyN << endl;
		cout << "  polySigma = " << para_farn_polySigma << endl;
		cout << "  flags = " << para_farn_flags << endl;
		break;
	case 3:
		cout << "  winSize = " << para_lk_winSize << endl;
		cout << "  maxLevel = " << para_lk_maxLevel << endl;
		cout << "  iters = " << para_lk_iters << endl;
		cout << "  useInitialFlow = " << para_lk_useInitialFlow << endl;
		break;
	}
	cout << "-----------------------" << endl;

	/***************************************/
	// SETUP INPUT
	/***************************************/

    printShortCudaDeviceInfo(getDevice());

    bool read_from_video;
    std::vector<fs::path> input_files;
    if(!fs::exists(input_path))
    {
    	cerr << "Invalid input path: " << input_path << endl;
    	return 1;
    }
    else
    {
    	if(fs::is_directory(input_path))
    	{
    		read_from_video = false;

    		// get list of image files in input_path
    		input_files = get_image_files(input_path, image_file_ext);

    		cout << "Found " << input_files.size() << " input images." << endl;
    	}
    	else
    		read_from_video = true;
    }

    if(limit_frames < 0)
    	limit_frames = input_files.size();  // max N of frames


    /***************************************/
	// SETUP OUTPUT PATHS
	/***************************************/

    if(!fs::exists(output_path / "u"))
    {
    	fs::create_directories(output_path / "u");
    }

    if(!fs::exists(output_path / "v"))
	{
		fs::create_directories(output_path / "v");
	}

    if(save_hsv && !fs::exists(output_path / "hsv"))
    {
    	fs::create_directories(output_path / "hsv");
    }

    const fs::path video_out = output_path / "output.avi";
    boost::format file_out_u((output_path / "u" / "frame_%08d.jpg").string());
    boost::format file_out_v((output_path / "v" / "frame_%08d.jpg").string());
    boost::format file_out_hsv((output_path / "hsv" / "frame_%08d.jpg").string());

    if(debug){
    	int disp_pos = (output_size > 0) ? output_size + 5 : 580;
		cv::namedWindow("in", CV_WINDOW_AUTOSIZE);
		cv::moveWindow("in", 0, 0);
		cv::namedWindow("out", CV_WINDOW_AUTOSIZE);
		cv::moveWindow("out", disp_pos, 0);
		cv::namedWindow("u", CV_WINDOW_AUTOSIZE);
		cv::moveWindow("u", 0, disp_pos + 20);
		cv::namedWindow("v", CV_WINDOW_AUTOSIZE);
		cv::moveWindow("v", disp_pos, disp_pos + 20);
    }

    /***************************************/
    // SETUP FLOW ALGORITHM
    /***************************************/

    /* parameters:
		- alpha: flow smoothness
		- gamma: gradient constancy importance
		- scale_factor: pyramid scale factor
		- inner_iterations: lagged non-linearity iterations
		- outer_iterations warping iterations (number of pyramid levels)
		- solver_iterations:  linear solver solver iterations
	*/
//	Ptr<cuda::BroxOpticalFlow> brox = cuda::BroxOpticalFlow::create(0.197f, 50.0f, 0.8f, 10, 77, 10);


    bool is_flow_float = false;
    Ptr<cuda::DenseOpticalFlow> alg;
    switch(alg_type){
    case 0:
    default:
    	// TVL1 flow algorithm
		alg = cuda::OpticalFlowDual_TVL1::create(
				para_tvl1_tau,
				para_tvl1_lambda,
				para_tvl1_theta,
				para_tvl1_nscales,
				para_tvl1_warps,
				para_tvl1_eps,
				para_tvl1_iterations,
				para_tvl1_scale_step,
				para_tvl1_gamma,
				para_tvl1_useInitialFlow);
    	break;

    case 1:
    	alg = cuda::BroxOpticalFlow::create(
				para_brox_alpha,
				para_brox_gamma,
				para_brox_scale,
				para_brox_inner_iter,
				para_brox_outer_iter,
				para_brox_solver_iter);
    	is_flow_float = true;
    	break;
    case 2:
    	alg = cuda::FarnebackOpticalFlow::create(
    			para_farn_numLevels,
    			para_farn_pyrScale,
    			para_farn_fastPyramids,
    			para_farn_winSize,
    			para_farn_numIters,
    			para_farn_polyN,
    			para_farn_polySigma,
    			para_farn_flags);
    	break;
    case 3:
    	alg = cuda::DensePyrLKOpticalFlow::create(
    			cv::Size(para_lk_winSize, para_lk_winSize),
    			para_lk_maxLevel,
    			para_lk_iters,
    			para_lk_useInitialFlow);
    	break;
    }

    /***************************************/
    // SETUP MATRICES
    /***************************************/

	// input frame from video/file stream
	Mat inframe;

	// first and second frame on GPU
	GpuMat d_frame0f;
	GpuMat d_frame1f;

	// flow in u and v direction, float
	GpuMat d_planes[2];

	// flow in u and v direction, clipped and CV_8U
	GpuMat d_img_u, d_img_v;

	// flow in u and v direction, clipped and CV_8U, CPU
	Mat img_u, img_v;

	/******************************************************************************/
	// VIDEO
	/******************************************************************************/

    if(read_from_video)
    {
		cout << "Setup video reader" << endl;
		cv::VideoCapture reader(input_path.string());

		if (!reader.isOpened())
		{
			cerr << "Can't open input video file" << endl;
			return -1;
		}

		// load first frame
		if (!reader.read(inframe))
		{
			cerr << "Failed to load first video frame..." << endl;
			return -1;
		}

		// upload first frame to gpu
		cv::cvtColor(inframe, inframe, cv::COLOR_BGR2GRAY);
		inframe.convertTo(inframe, CV_32FC1, 1.0 / 255.0);
		d_frame0f.upload(inframe);

		// instatiate output matrix for flow computation with the right size and dtype
		GpuMat d_flow(inframe.size(), CV_32FC2);

		cv::Ptr<cv::VideoWriter> writer;
		if(is_video_out)
		{
			cout << "Setup video writer" << endl;
			// X264
			writer = cv::makePtr<cv::VideoWriter>(cv::VideoWriter(video_out.string(),
					cv::VideoWriter::fourcc('X', '2', '6', '4'), 25., inframe.size(), true));
		}

		cout << "Processing frames ..." << endl;
		const int64 start_tick = getTickCount();
		double timeSec = 0;
		float fps = 0;

		size_t i;
		Mat out, flow;
		for (i = 1; i < limit_frames; ++i)
		{
			// ----------------------------------------------------
			// PREPARE INPUT

			// shift previous frame
			d_frame0f.swap(d_frame1f);

			// read next frame
			if (!reader.read(inframe))
				break;

			// convert to single channel gray
			cv::cvtColor(inframe, inframe, cv::COLOR_BGR2GRAY);
			// scale to [0,1]
			inframe.convertTo(inframe, CV_32FC1, 1.0 / 255.0);

			// upload to gpu
			d_frame0f.upload(inframe);

			// ----------------------------------------------------
			// COMPUTE FLOW

			alg->calc(d_frame1f, d_frame0f, d_flow);

			// ----------------------------------------------------
			// OUTPUT

			// split flow to 2-channels, u and v
			cuda::split(d_flow, d_planes);

			// convert to image by clipping and scaling
			convertFlowToImage(d_planes[0], d_img_u, clip_value);
			convertFlowToImage(d_planes[1], d_img_v, clip_value);

			d_img_u.download(img_u);
			d_img_v.download(img_v);

			if(is_video_out)
			{
				// just for visualization: draw fancy flow colors
				drawOpticalFlow(img_u, img_v, out);
				writer->write(out);
			}
			else
			{
				cv::imwrite((file_out_u % i).str(), img_u);
				cv::imwrite((file_out_v % i).str(), img_v);
			}

			if(i % 100 == 0)
			{
				// some status update
				timeSec = (getTickCount() - start_tick) / getTickFrequency();
				fps = i / timeSec;
				cout << i << ": " << timeSec << " sec" << ", avg. " << fps << "fps" << endl;
			}
		}

		cout << "Done. Computed flow of " << i << " frames." << endl;

		reader.release();
		if(writer)
			writer->release();
    }
    else
    {
    	/******************************************************************************/
    	// IMAGE FILES
    	/******************************************************************************/

    	float bf_sigma = 50;
    	int bf_kernel = 3;

    	std::vector<cuda::GpuMat> input_queue;
		size_t queue_size = skip_frames + 1;

		// read first frame in grayscale
		cout << "First frame: " << input_path / input_files.front() << endl;

		// fill initial queue
		for (size_t i = 0; i < queue_size; ++i)
		{
			inframe = cv::imread((input_path / input_files.at(i)).string(), cv::IMREAD_GRAYSCALE);

			// scale to [0, 1]
			if(is_flow_float)
				inframe.convertTo(inframe, CV_32FC1, 1.0 / 255.0);

			// upload to gpu (will shift frame 1 to 0 in the loop)
			d_frame0f.upload(inframe);

			// a bit of smoothing
			cuda::bilateralFilter(d_frame0f, d_frame1f, bf_kernel, bf_sigma, bf_sigma);

			// resize if wanted
			if(output_size > 0)
				cuda::resize(d_frame1f, d_frame1f, Size(output_size, output_size), 0, 0, cv::INTER_CUBIC);

			input_queue.push_back(d_frame1f.clone());
		}

		// matrix for flow computation output in right size and dtype
		GpuMat d_flow(inframe.size(), CV_32FC2);
		GpuMat d_norm_u, d_norm_v;
		cv::Mat flow_hsv;

		cout << "Processing frames ..." << endl;
		const int64 start_tick = getTickCount();
		double timeSec = 0;
		float fps = 0;
		size_t count = 0;
		std::deque<cv::Point2f> mean_vecs;

		// loop over images starting with second image until limit or exhausted
    	for (size_t i = 1; i <= limit_frames && i <= (input_files.size() - skip_frames); ++i)
    	{
			// ----------------------------------------------------
			// COMPUTE FLOW

    		// first and last in the queue
			alg->calc(input_queue.front(), input_queue.back(), d_flow);

			// ----------------------------------------------------
			// OUTPUT

			// split flow to 2-channels, u and v
			cuda::split(d_flow, d_planes);

			cv::Point2f principal_motion;
			normalizeFlow(d_planes[0], d_planes[1], d_norm_u, d_norm_v, mean_vecs, principal_motion,
					norm_win_size, norm_pixel_flow_thresh, norm_global_flow_thresh);

			// convert to image by clipping and scaling
			convertFlowToImage(d_norm_u, d_img_u, clip_value);
			convertFlowToImage(d_norm_v, d_img_v, clip_value);

			d_img_u.download(img_u);
			d_img_v.download(img_v);

			if(save_hsv){
				double min_u, max_u, min_v, max_v;
				cuda::minMax(d_norm_u, &min_u, &max_u);
				cuda::minMax(d_norm_v, &min_v, &max_v);
				float max_motion = max(10., sqrt(max_u * max_u + max_v * max_v));

				drawOpticalFlow(d_norm_u, d_norm_v, flow_hsv, max_motion);

				cv::imwrite((file_out_hsv % i).str(), flow_hsv);
			}

			if(debug){

				if(!save_hsv) { // if save_hsv, we have already computed the HSV image
					double min_u, max_u, min_v, max_v;
					cuda::minMax(d_norm_u, &min_u, &max_u);
					cuda::minMax(d_norm_v, &min_v, &max_v);
					float max_motion = max(10., sqrt(max_u * max_u + max_v * max_v));

					// display flow before clipping, apply own clipping (max_motion)
					drawOpticalFlow(d_norm_u, d_norm_v, flow_hsv, max_motion);
				}

				// draw principal motion on source image
				input_queue.front().download(inframe);
				cv::Point2f center(inframe.rows/2, inframe.cols/2);
				cv::line(inframe, center - (principal_motion*inframe.rows/5), center + (principal_motion*inframe.rows/5), cv::Scalar::all(255), 1, 8, 0);

//				float motion_phi = atan2f(principal_motion.y, principal_motion.x) * 180.f / CV_PI;
//				cv::Mat M = cv::getRotationMatrix2D(cv::Point2f(inframe.rows/2., inframe.cols/2.), motion_phi-90, 1);
//				cv::Mat outframe;
//				cv::warpAffine(inframe, outframe, M, inframe.size(), INTER_CUBIC + WARP_FILL_OUTLIERS, BORDER_CONSTANT, cv::Scalar(0));

				cv::imshow("in", inframe);
				cv::imshow("out", flow_hsv);
				cv::imshow("u", img_u);
				cv::imshow("v", img_v);
				cv::waitKey(10);
			}

			cv::imwrite((file_out_u % i).str(), img_u);
			cv::imwrite((file_out_v % i).str(), img_v);

//				fwrite(&min_u_f,sizeof(float),1,fx);
//				fwrite(&max_u_f,sizeof(float),1,fx);
//				fwrite(&min_v_f,sizeof(float),1,fx);
//				fwrite(&max_v_f,sizeof(float),1,fx);

			count++;

			if(count % 100 == 0)
			{
				// some status update
				timeSec = (getTickCount() - start_tick) / getTickFrequency();
				fps = count / timeSec;
				cout << count << ": " << timeSec << " sec" << ", avg. " << fps << "fps" << endl;
			}

			// ----------------------------------------------------
			// PREPARE NEXT INPUT

			if((i+skip_frames) >= input_files.size())
				break;

			for(size_t j = 0; j < queue_size - 1; ++j) {
				// move everything forward by one
				input_queue.at(j).swap(input_queue.at(j+1));
			}

			// read next image and scale to [0,1]
			inframe = cv::imread((input_path / input_files.at(i + skip_frames)).string(), cv::IMREAD_GRAYSCALE);

			if(is_flow_float)
				inframe.convertTo(inframe, CV_32FC1, 1.0 / 255.0);

			// upload to GPU
			d_frame0f.upload(inframe);

			// bit of smoothing
			cuda::bilateralFilter(d_frame0f, d_frame1f, bf_kernel, bf_sigma, bf_sigma);

			// resize if needed
			if(output_size > 0)
				cuda::resize(d_frame1f, d_frame1f, Size(output_size, output_size), 0, 0, cv::INTER_CUBIC);

			// set as last in queue
			d_frame1f.copyTo(input_queue.back());
    	}

    	timeSec = (getTickCount() - start_tick) / getTickFrequency();
    	fps = count / timeSec;
    	cout << "Done (" << timeSec << " sec" << ", avg. " << fps << "fps)." << endl;
    }
    return 0;
}

