#include "utils_flow.h"

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include "opencv2/cudaarithm.hpp"

// Namespaces
using namespace std;
using namespace cv;

void convertFlowToImage(const Mat &flowIn, Mat &flowOut, float bound) {

	double alpha, beta;
	if(bound <= 0) {
		double min_u, max_u;
		cv::minMaxLoc(flowIn, &min_u, &max_u);
		alpha = 255. / (max_u - min_u);
		beta = min_u * alpha;
	}
	else {
		// symmetric bounds: because lowerBound = -higherBound, we can simply scale and translate such that
		//  lowerBound := 0 and upperBound := 255.
		alpha = 128./bound;
		beta = 128.;
	}

	//  convertTo will apply saturated cast and therefore clip everything below and above
	flowIn.convertTo(flowOut, CV_8UC2, alpha, beta);
}

void convertFlowToImage(const cv::cuda::GpuMat &flowIn, cv::cuda::GpuMat &flowOut, float bound) {

	double alpha, beta;
	if(bound <= 0) {
		double min_u, max_u;
		cv::cuda::minMax(flowIn, &min_u, &max_u);
		alpha = 255. / (max_u - min_u);
		beta = min_u * alpha;
	}
	else {
		// symmetric bounds: because lowerBound = -higherBound, we can simply scale and translate such that
		//  lowerBound := 0 and upperBound := 255.
		alpha = 128./bound;
		beta = 128.;
	}

	//  convertTo will apply saturated cast and therefore clip everything below and above
	flowIn.convertTo(flowOut, CV_8UC2, alpha, beta);
}

void normalizeFlow(const cv::cuda::GpuMat& flow_u, const cv::cuda::GpuMat& flow_v, cv::cuda::GpuMat& norm_flow_u, cv::cuda::GpuMat& norm_flow_v,
		std::deque<cv::Point2f>& mean_vecs, cv::Point2f& principal_motion,
		const int norm_win_size, const float norm_pixel_flow_thresh, const float norm_global_flow_thresh)
{
	/* Step 1:
	 * 	- Compute mean flow per direction u, v
	 * 	- Compute the angle of the mean flow
	 * Step 2:
	 *  - Rotate all flow vectors by the mean angle (have mean flow point in +y direction, so downwards in the image)
	 *  - Rotate the flow images such that u, v are aligned again with x, y
	 *  - Clip and fill borders while rotating
	 */

	const bool debug = false;

	// Step 1

	// make all flow vectors point in positive v-direction (flip u/v if not)
//	cuda::GpuMat v_mask;
//	cuda::compare(flow_v, 0.f, v_mask, cv::CMP_LT);  // find vectors with v < 0
//	cuda::GpuMat sign_v(flow_u.size(), CV_32FC1, 1.f);
//	sign_v.setTo(-1.f, v_mask);
//	cuda::multiply(flow_u, sign_v, norm_flow_u);  // * -1 where v < 0
//	cuda::abs(flow_v, norm_flow_v);  // make all v >= 0

	cuda::abs(flow_u, norm_flow_u);
	cuda::abs(flow_v, norm_flow_v);

	// compute mean flow in u and v
	cuda::GpuMat mag, mag_mask;
	cuda::magnitude(norm_flow_u, norm_flow_v, mag);
	cuda::compare(mag, norm_pixel_flow_thresh, mag_mask, cv::CMP_GE);
	float mask_cnt = cuda::countNonZero(mag_mask);

//	cv::Mat c_mask;
//	mag_mask.download(c_mask);
//	cv::namedWindow("mask", 1);
//	cv::imshow("mask", c_mask);

	if(mean_vecs.size() > 0 && mean_vecs.size() >= norm_win_size)
	{
		mean_vecs.pop_front();
	}

	std::stringstream info;
	info.precision(4);

	if(debug)
		info << "|flow|: ";

	if (mask_cnt > 0) // any pixel flow > threshold
	{
//		cv::Mat c_flow_hist;
//
//		cv::Mat c_flow_u, c_flow_v, c_mask;
//		norm_flow_u.download(c_flow_u);
//		norm_flow_v.download(c_flow_v);
//		v_mask.download(c_mask);
//		cv::Mat c_flow;
//		std::vector<cv::Mat> c_flow_vec;
//		c_flow_vec.push_back(c_flow_u);
//		c_flow_vec.push_back(c_flow_v);
//		cv::merge(c_flow_vec, c_flow);
//
//	    int ubins = 7, vbins = 7;
//	    int histSize[] = {ubins, vbins};
//	    // hue varies from 0 to 179, see cvtColor
//	    float uranges[] = { -3, 3 };
//	    // saturation varies from 0 (black-gray-white) to
//	    // 255 (pure spectrum color)
//	    float vranges[] = { 0, 3 };
//	    const float* ranges[] = { uranges, vranges };
//	    int channels[] = {0, 1};
//
//		cv::calcHist(&c_flow, 1, channels, c_mask, c_flow_hist, 2, histSize, ranges, true, false);
//
////	    double maxVal=0;
////	    cv::minMaxLoc(c_flow_hist, 0, &maxVal, 0, 0);
//	    int scale = 10;
//	    cv::Mat histImg = Mat::zeros(ubins*scale, vbins*10, CV_8UC3);
//	    for( int h = 0; h < ubins; h++ )
//	        for( int s = 0; s < vbins; s++ )
//	        {
//	            float binVal = c_flow_hist.at<float>(h, s);
//	            int intensity = cvRound(binVal*255/float(mask_cnt/10));
//	            cv::rectangle( histImg, cv::Point(h*scale, s*scale),
//	            			cv::Point( (h+1)*scale - 1, (s+1)*scale - 1),
//	                        cv::Scalar::all(intensity),
//	                        CV_FILLED );
//	        }
//
//	    namedWindow( "source", 1 );
//	    namedWindow( "H-S Histogram", 1 );
//	    imshow( "H-S Histogram", histImg );
//	    imshow( "source", c_mask );


		float mean_flow_u = cuda::sum(norm_flow_u, mag_mask)[0] / mask_cnt;
		float mean_flow_v = cuda::sum(norm_flow_v, mag_mask)[0] / mask_cnt;

		float mag_flow = std::sqrt(mean_flow_u*mean_flow_u + mean_flow_v*mean_flow_v);
		info << mag_flow;

		if (mag_flow > norm_global_flow_thresh){
			// remember mean flow for next iterations
			mean_vecs.push_back( cv::Point2f(mean_flow_u, mean_flow_v) );
		}
	}
	else // no motion in this frame
		if(debug)
			info << "-.----";

	// compute average mean flow over previous frames
	if(mean_vecs.size() > 0)
		principal_motion = std::accumulate(mean_vecs.begin(), mean_vecs.end(), cv::Point2f(0.f, 0.f)) / float(mean_vecs.size());
	else
		principal_motion = cv::Point2f(0, 1);

	// compute angle of principal motion
	float motion_phi = atan2f(principal_motion.y, principal_motion.x) * 180.f / CV_PI;

	if(debug) {
		info << ", |mot'|: " << cv::norm(principal_motion);
		info << ", phi': " << motion_phi;
		info << ", mot': " << principal_motion;
	}

	// Step 2
	cuda::GpuMat ang;
	cuda::GpuMat flow_u_rot, flow_v_rot;
	cuda::cartToPolar(flow_u, flow_v, mag, ang, true);  // to polar
	cuda::subtract(ang, 90-motion_phi, ang);  // make principal motion direction 90 deg (+y)
	cuda::polarToCart(mag, ang, flow_u_rot, flow_v_rot, true);  // back to cartesian

	// rotate flow images, filling borders with 0
	// rotate such that principal motion axis points +y
	cv::Mat M = cv::getRotationMatrix2D(cv::Point2f(flow_u.rows/2., flow_u.cols/2.), motion_phi-90, 1);
	cuda::warpAffine(flow_u_rot, norm_flow_u, M, flow_u.size(), INTER_CUBIC + WARP_FILL_OUTLIERS, BORDER_CONSTANT, cv::Scalar(0));
	cuda::warpAffine(flow_v_rot, norm_flow_v, M, flow_u.size(), INTER_CUBIC + WARP_FILL_OUTLIERS, BORDER_CONSTANT, cv::Scalar(0));

	if(debug)
		std::cout << info.str() << std::endl;
}

inline bool isFlowCorrect(Point2f u)
{
    return !cvIsNaN(u.x) && !cvIsNaN(u.y) && fabs(u.x) < 1e9 && fabs(u.y) < 1e9;
}

Vec3b computeColor(float fx, float fy)
{
    static bool first = true;

    // relative lengths of color transitions:
    // these are chosen based on perceptual similarity
    // (e.g. one can distinguish more shades between red and yellow
    //  than between yellow and green)
    const int RY = 15;
    const int YG = 6;
    const int GC = 4;
    const int CB = 11;
    const int BM = 13;
    const int MR = 6;
    const int NCOLS = RY + YG + GC + CB + BM + MR;
    static Vec3i colorWheel[NCOLS];

    if (first)
    {
        int k = 0;

        for (int i = 0; i < RY; ++i, ++k)
            colorWheel[k] = Vec3i(255, 255 * i / RY, 0);

        for (int i = 0; i < YG; ++i, ++k)
            colorWheel[k] = Vec3i(255 - 255 * i / YG, 255, 0);

        for (int i = 0; i < GC; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255, 255 * i / GC);

        for (int i = 0; i < CB; ++i, ++k)
            colorWheel[k] = Vec3i(0, 255 - 255 * i / CB, 255);

        for (int i = 0; i < BM; ++i, ++k)
            colorWheel[k] = Vec3i(255 * i / BM, 0, 255);

        for (int i = 0; i < MR; ++i, ++k)
            colorWheel[k] = Vec3i(255, 0, 255 - 255 * i / MR);

        first = false;
    }

    const float rad = sqrt(fx * fx + fy * fy);
    const float a = atan2(-fy, -fx) / (float) CV_PI;

    const float fk = (a + 1.0f) / 2.0f * (NCOLS - 1);
    const int k0 = static_cast<int>(fk);
    const int k1 = (k0 + 1) % NCOLS;
    const float f = fk - k0;

    Vec3b pix;

    for (int b = 0; b < 3; b++)
    {
        const float col0 = colorWheel[k0][b] / 255.0f;
        const float col1 = colorWheel[k1][b] / 255.0f;

        float col = (1 - f) * col0 + f * col1;

        if (rad <= 1)
            col = 1 - rad * (1 - col); // increase saturation with radius
        else
            col *= .75; // out of range

        pix[2 - b] = static_cast<uchar>(255.0 * col);
    }

    return pix;
}

void drawOpticalFlow(const cv::Mat_<float>& flowx, const cv::Mat_<float>& flowy, cv::Mat& dst, float maxmotion)
{
    dst.create(flowx.size(), CV_8UC3);
    dst.setTo(Scalar::all(0));

    // determine motion range:
    float maxrad = maxmotion;

    if (maxmotion <= 0)
    {
        maxrad = 1;
        for (int y = 0; y < flowx.rows; ++y)
        {
            for (int x = 0; x < flowx.cols; ++x)
            {
                Point2f u(flowx(y, x), flowy(y, x));

                if (!isFlowCorrect(u))
                    continue;

                maxrad = max(maxrad, sqrt(u.x * u.x + u.y * u.y));
            }
        }
    }

    for (int y = 0; y < flowx.rows; ++y)
    {
        for (int x = 0; x < flowx.cols; ++x)
        {
            Point2f u(flowx(y, x), flowy(y, x));

            if (isFlowCorrect(u))
                dst.at<Vec3b>(y, x) = computeColor(u.x / maxrad, u.y / maxrad);
        }
    }
}

void drawOpticalFlow(const cv::cuda::GpuMat& flowx, const cv::cuda::GpuMat& flowy, cv::Mat& dst, float maxmotion)
{
	// stupid way
	cv::Mat cpu_flowx(flowx);
	cv::Mat cpu_flowy(flowy);

	drawOpticalFlow(cpu_flowx, cpu_flowy, dst, maxmotion);
}


cv::Mat get_histogram(const cv::Mat& img, bool uniform, bool accumulate)
{
	/// Establish the number of bins
	int histSize = 256;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 } ;
	const float* histRange = { range };

	Mat hist;

	/// Compute the histograms:
	calcHist( &img, 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate );

	// Draw the histograms for B, G and R
	int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound( (double) hist_w/histSize );

	Mat histImage( hist_h, hist_w, CV_8UC3, Scalar(0,0,0) );

	/// Normalize the result to [ 0, histImage.rows ]
	normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

	/// Draw for each channel
	for( int i = 1; i < histSize; i++ )
	{
		line( histImage, Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ) ,
				Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
				Scalar( 255, 255, 255), 2, 8, 0  );
	}

	return histImage;
}
