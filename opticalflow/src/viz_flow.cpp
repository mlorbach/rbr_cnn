/*
 * viz_flow.cpp
 *
 *  Created on: Feb 27, 2017
 *      Author: malte
 */

#include <iostream>
#include <fstream>

#include "opencv2/core.hpp"
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "utils_flow.h"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

int main(int argc, const char* argv[])
{
	fs::path input_path, input_path_orig;
	bool debug;

	/***************************************/
	// COMMAND LINE ARGUMENTS
	/***************************************/

	// Declare the supported options.
	po::options_description desc("Usage");
	desc.add_options()
	    ("help,h", "produce help message")
	    ("flow", po::value<fs::path>(&input_path)->default_value("output")->required(), "Flow images (expect subfolder 'u' and 'v')")
	    ("original", po::value<fs::path>(&input_path_orig), "Original images")
	    ("debug,d", po::bool_switch(&debug)->default_value(false), "Show debug output.")
	;

	po::positional_options_description pos_args;
	pos_args.add("flow", 1);
	pos_args.add("original", 1);

	po::variables_map vm;

	try {
		po::store(po::command_line_parser(argc, argv).options(desc).positional(pos_args).run(), vm);

		if (vm.count("help")) {
			std::cout << desc << std::endl;
			return 1;
		}

		po::notify(vm);
	}
	catch(po::error& e) {
		std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
		std::cerr << desc << std::endl;
		return 1;
	}

	fs::path path_u = input_path / "u";
	fs::path path_v = input_path / "v";

	if(!fs::exists(path_u) || !fs::is_directory(path_u) || !fs::exists(path_v) || !fs::is_directory(path_v)) {
		std::cerr << "Could not find flow folders 'u' and 'v'." << std::endl;
		return 1;
	}

	// read all files and sort
	std::vector<fs::path> files_u, files_v, files_orig;
	copy(fs::directory_iterator(path_u), fs::directory_iterator(), back_inserter(files_u));
	sort(files_u.begin(), files_u.end());
	copy(fs::directory_iterator(path_v), fs::directory_iterator(), back_inserter(files_v));
	sort(files_v.begin(), files_v.end());

	bool show_original = fs::is_directory(input_path_orig);

	const std::string ext = ".jpg";
	const float max_motion = sqrt(2.)/2.; // == sqrt(.5**2 + .5**2)
	char k;
	bool cancel = false;

	if(show_original)
		cv::namedWindow("original", cv::WINDOW_AUTOSIZE);
	cv::namedWindow("flow", cv::WINDOW_AUTOSIZE);
	cv::moveWindow("flow", 600,400);


	cv::Mat flow_u, flow_v, flow_rgb, original;
	for(size_t i = 0; i < files_u.size() && !cancel; ++i)
	{
		if(files_u.at(i).extension() == ext && files_v.at(i).extension() == ext)
		{
			if(debug)
				std::cout << "Loading " << files_u.at(i) << " and " << files_v.at(i) << std::endl;

			flow_u = cv::imread(files_u.at(i).string(), cv::IMREAD_GRAYSCALE);
			flow_v = cv::imread(files_v.at(i).string(), cv::IMREAD_GRAYSCALE);

			flow_u.convertTo(flow_u, CV_32F, 1/255., -0.5);
			flow_v.convertTo(flow_v, CV_32F, 1/255., -0.5);

			drawOpticalFlow(flow_u, flow_v, flow_rgb, 0.25);

			cv::imshow("flow", flow_rgb);

			if(show_original) {
				original = cv::imread((input_path_orig / files_u.at(i).filename()).string(), cv::IMREAD_GRAYSCALE);
				cv::imshow("original", original);
			}

			k = cv::waitKey( debug ? 0 : 40 );
			switch(k){
			case 27:
			case 'q':
				cancel = true;
				break;
			}
		}
	}

	return 0;
}
