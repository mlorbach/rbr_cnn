Rodent tracking with Caffe CNN
==============================


Prerequisites
-------------

# System #

All instructions in this document are for a Debian-based Linux OS. I've used both Ubuntu 16.04 and Linux Mint 18. Both should work fine.


## SSH ##
If you're working on a brand-new headless system, setup the ssh-server first.

From: http://ubuntuhandbook.org/index.php/2016/04/enable-ssh-ubuntu-16-04-lts

```sh
sudo apt install openssh-client openssh-server
# check the status
sudo service ssh status
# configure
sudo nano /etc/ssh/sshd_config
# reload service
sudo service ssh restart
```


## Essentials ##

`sudo apt-get install build-essential checkinstall git cmake cmake-curses-gui`


# CUDA #

* Website: https://developer.nvidia.com/cuda-toolkit
* Installation guide Linux: [PDF](http://developer.download.nvidia.com/compute/cuda/8.0/secure/Prod2/docs/sidebar/CUDA_Installation_Guide_Linux.pdf?autho=1487769185_a7805c3ab5d2e33fe97daa0c77819ce7&file=CUDA_Installation_Guide_Linux.pdf)
* Download the runfile installer for CUDA 8.0 from https://developer.nvidia.com/cuda-downloads
* Download the display driver (current version is 375.39) from http://www.nvidia.com/Download/index.aspx
* Download the cuDNN library (current version 5.1) from https://developer.nvidia.com/cudnn



## Pre-checks ##

* Check if your GPU is detected properly: `lspci | grep -i nvidia` should show you graphics card.
* Check gcc compiler: `gcc --version` should be >= 5.3.1
* Install kernel headers: `sudo apt-get install linux-headers-$(uname -r)`

We will follow the runfile installation of both drivers and CUDA toolkit. That is, we will not be using the distro .deb files. We need to make sure that we have not any Nvidia drivers or CUDA files from the distro repos installed. If you have any nvidia stuff installed, remove it first:  `sudo apt-get --purge remove nvidia-*`


## Install nvidia drivers and CUDA Toolkit ##

1. Disable nouveau drivers before installing nvidia drivers. Create a file at `/etc/modprobe.d/blacklist-nouveau.conf` with the following contents:

```
blacklist nouveau
options nouveau modeset=0
```

2. Regenerate the kernel initramfs: `sudo update-initramfs -u`
3. Reboot into text mode (runlevel 3)
   * This can usually be accomplished by adding the number "3" to the end of the system's kernel boot parameters. Possibly, you may also need to add "nomodeset".
4. Install the nvidia drivers: `sudo sh NVIDIA-Linux-x86_64-375.39.run`
   * Say yes, if asked to create a X server configuration
5. Install the CUDA toolkit: `sudo sh cuda_8.0.61_375.26_linux-run`
   * When asked whether to install drivers, say NO!
7. Reboot
8. Check if things work
   * Check the correct driver version is loaded: `cat /proc/driver/nvidia/version`

## cuDNN ##

Install cudnn library for accelerated Deep Neural Network training.

```sh
tar -xvf cudnn-8.0-linux-x64-v5.1.tgz -C cudnn
sudo cp cudnn/cuda/* /usr/local/cuda
rm -r cudnn
```


# OpenCV #

* Required: OpenCV 3 (I used 3.2.0)
* Download: `wget -c https://github.com/opencv/opencv/archive/3.2.0.zip`
* Or clone master: `git clone https://github.com/opencv/opencv.git opencv-dev`
* Compile:

```sh
cd opencv-dev
mkdir build
cd build
ccmake ..
```

* Configure CMake according to your system. Make sure to:
  * BUILD_TIFF=ON
  * WITH_CUDA=ON
  * WITH_PYTHON=ON
  * You may switch off BUILD_TEST and BUILD_PERFTEST for faster compilation
  * You may also remove some of the CUDA architectures for which to compile (https://developer.nvidia.com/cuda-gpus) to speed up compilation
     * Titan X (Pascal) is version 61
     * Quadro K2000 is 30
     * NVS K1100M is 30

```sh
make -j7
make install
```


# Installing Caffe #

Some random notes

* If you are using Anaconda, keep your root conda environment as clean as possible.
  * In particular avoid installing these packages: hdf5, libprotobuf.
  * You'll need those to use pycaffe later on, but it's better to install them in a separate conda environment.
  * Why? Because the caffe Makefile/cmake is a bit unpredictable in which libraries it finds and includes. It tends to use e.g. the libhdf5's it finds in the Anaconda directory rather than the system libraries. If you link against the Anaconda ones, you'll run into library conflicts later on. Don't do it.
  * Make sure everything that cmake finds, are system libraries and not local anaconda libraries (check for any paths that include /home/mysuser/anaconda2/...).
* I had troubles with OpenCV while compiling Caffe. There were some errors related to undefined libTIFF references. I solved it by compiling OpenCV with BUILD_TIFF=ON.

## Required libraries ##

* Check latest installation documentation: http://caffe.berkeleyvision.org/install_apt.html
* Be careful, the official docs say to install libopencv-dev. We have compiled it ourselves, so don't install the deb-version~
* `sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler`
* `sudo apt-get install --no-install-recommends libboost-all-dev`

## Compilation ##

1. Check out latest code `git clone https://github.com/BVLC/caffe.git ~/Install/caffe`
2. Check out latest rbr_cnn `git clone git@gitlab.com:mlorbach/rbr_cnn.git ~/rbr_cnn`
3. Copy the imageset for regression tool: `cp ~/rbr_cnn/caffe/tools/* ~/Install/caffe/tools`
4. Apply a patch for multi-batch HDF5 output laters

```sh
cp ~/rbr_cnn/caffe/5237_multiHDF5Output.patch ~/Install/caffe
cd ~/Install/caffe
git am 5237_multiHDF5Output.patch
```

5. Configure and build

```sh
cd ~/Install/caffe
mkdir build
cd build
ccmake ..
make all -j7
make install
```


# Python via Anaconda #

* Download anaconda installer for Python 2.7: https://www.continuum.io/downloads#linux
* Install `bash Anaconda2-<version>-Linux-x86_64.sh`


## Set up Python for using pycaffe ##
* After you have sucessfully compiled Caffe and installed anaconda
* Note that this creates a Python environment for _using_ pycaffe, not for compiling it!
* Create a conda environment:

  ```sh
  cd rbr_cnn
  conda env create -n rbr_cnn -f=environment.yml
  ```
* To be able to use the caffe python package (pycaffe), we need to add the path containing the package into our python environment
  * To do it on the fly, do

  	```python
  	import sys
  	sys.path.append('<caffe_root_dir>/python')
  	import caffe
  	```
  * For a more permanent and easier solution, include it in your site-packages. Note that this may not be the best solution as it involves manually adding stuff to your Python environment. Those changes will not translate to other PCs etc, so it's not good for deployment of code. But for our purposes I think this is okay.

    `echo "<caffe_root_dir>/python" > ~/anaconda2/envs/<your_env_name>/lib/python2.7/site-packages/caffe.pth`
    * For example, in my case this is:

    `echo "/home/malte/Downloads/caffe/python" > ~/anaconda2/envs/rbr_cnn/lib/python2.7/site-packages/caffe.pth`
* Copy OpenCV python library to new environment:

  ```
  cd ~/Install/opencv-dev/build
  cp lib/cv2.so ~/anaconda2/envs/rbr_cnn/lib/python2.7/site-packages/
  # if you want opencv to be available in your global Python environment, also copy it to
  sudo cp lib/cv2.so /usr/local/lib/python2.7/dist-packages/
  ```


Input data
----------

* x264 videos (mp4) with resolution 576x576
  * The set contains two recording setups, which are not aligned. That is the cage center and corners are different depending on the setup.


## Converting videos to images ##
Use the script `scripts/convert_videos_to_images.sh`. Check the parameters to set within the script before you run it.

## Create lmdb for training, test and validation sets ##
`scripts/make_ratsi_dataset.sh`

