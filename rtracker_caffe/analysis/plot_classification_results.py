
# coding: utf-8

# In[2]:

# set up Python environment: numpy for numerical routines, and matplotlib for plotting
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import h5py
from natsort import natsorted, ns

from sklearn.metrics import confusion_matrix
from utils import classification_report

# Fixed paths
base_local = os.path.expanduser('~/rbr_cnn/rtracker_caffe/log')
base_rbr = os.path.expanduser('~/rbr_cnn/rtracker_caffe')

local_net_path = os.path.join(base_rbr,'net')
local_data_path = os.path.join(base_rbr,'data', 'data_flow_norm_rep')

# SET this
in_archive = True
SSH_server = 'thor'


# If true, load the video/frame index from the aux data (test.txt) or (test_set.txt if shuffled), 
#  so that we can assign predictions to their proper video segments. This is also used in case the test set is shuffled 
#  and to remove duplicate predictions due to batch processing.
# Set only to False if you are sure you know why you do it!
use_proper_index = True

# In[3]:

if not in_archive:
    local_model_path = os.path.join(base_rbr,'models')
    local_log_path = os.path.join(base_rbr,'log')
    
    def _load_ssh(btn):
        get_ipython().system(u'scp {SSH_server}:rbr_cnn/rtracker_caffe/log/ratsi_test.log {local_log_path}')
        get_ipython().system(u'scp {SSH_server}:rbr_cnn/rtracker_caffe/log/test_probs.h5 {local_log_path}')
    
    # load log file from server
    
    btn_load = widgets.Button(
        description='Load from SSH',
        disabled=False,
        button_style='', # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Load log and test files from SSH',
        icon='check'
    )

    display(btn_load)
    btn_load.on_click(_load_ssh)

else:
    folders = sorted(os.listdir(base_local), reverse=True)
    #sel_folder = widgets.Select(description='Results folder:', options=folders)
    #display(sel_folder)


# In[4]:

if in_archive:
    local_model_path = os.path.join(base_rbr, 'models')
    local_log_path = os.path.join(base_rbr, 'log')
    
# load labels
labels_file = os.path.join(local_data_path, 'labels.txt')
label_set = np.loadtxt(labels_file, str, delimiter='\t')

# load predictions
probs = []
labels = []

is_clips = False
probs_file = os.path.join(local_log_path, 'test_probs.h5')
if not os.path.isfile(probs_file):
    probs_file = os.path.join(local_log_path, 'test_probs_clip.h5')
    is_clips = True

with h5py.File(probs_file, 'r') as fh:    
    for key in natsorted(fh.keys(), alg=ns.IGNORECASE):
        if key.startswith('label'):
            labels.append(fh[key][()])
        else:
            probs.append(fh[key][()])
            
if is_clips:
    probs = np.concatenate(probs, axis=1).squeeze()
    probs = probs.mean(axis=0)
    labels = np.concatenate(labels, axis=1).squeeze().astype(int)
    labels = labels[0, :]
else:
    probs = np.vstack(probs).squeeze()
    labels = np.vstack(labels).squeeze().astype(int)


# labels used
used_labels = sorted(np.unique(labels))
label_set = label_set[used_labels]

pred = np.argmax(probs[:, used_labels], axis=1)

if use_proper_index:
    # Merge predictions with proper index
    set_file = os.path.join(local_data_path, 'test_index.txt')
    if not os.path.isfile(set_file):
        set_file = os.path.join(local_data_path, 'test.txt')
        print 'Using default test set index.'
    else:
        print 'Using de-randomized test set index.'

    # peek inside
    ncols = pd.read_csv(set_file, sep=' ', header=None, nrows=1).shape[1]
    if ncols == 2:
        # standard <image> <label> format
        df = pd.read_csv(set_file, sep=' ', header=None, names=['image', 'label'])
        # convert image path to video name and frame number
        df['video'] = df['image'].apply(lambda p: os.path.basename(os.path.dirname(p)))
        df['frame'] = df['image'].apply(lambda p: int(os.path.splitext(os.path.basename(p))[0].split('_')[-1]))
        df = df.drop('image', axis=1)

    elif ncols == 3:
        # C3D format: <folder> <start_frame> <label>
        df = pd.read_csv(set_file, sep=' ', header=None, names=['video', 'frame', 'label'])

    else:
        raise TypeError('Unknown index format. Neither 2 nor 3 column format.')

    df = df.set_index(['video', 'frame'])

    # check if results are shuffled
    if not (df['label'].iloc[:50] == labels[:50]).all():
        print 'Result is shuffled. Cannot align results properly. Using randomized results.'
        df = pd.DataFrame(pred[:df.shape[0]], columns=['label'])  # new dataframe without index
        df['pred'] = pred[:df.shape[0]] # copy predictions
        dfprobs = pd.DataFrame(index=df.index, data=probs[:df.shape[0], :])  
    else:
        # sanity check, size test set and predictions are unequal due to batch processing;
        # the additional prediction should be equal to the first predictions (batch processing start from beginning again)
        overlap_test = pred.shape[0] - df.shape[0]

#         if not (pred[:overlap_test] == pred[-overlap_test:]).all():
#             raise ValueError('Predictions invalid. Are they shuffled?')

        df['pred'] = pred[:df.shape[0]] # copy predictions
        dfprobs = pd.DataFrame(index=df.index, data=probs[:df.shape[0], :])

        df = df.sortlevel(0)
        dfprobs = dfprobs.sortlevel(0)
else:
    df = pd.DataFrame(np.vstack([labels, pred]).T, columns=['label', 'pred'])
    dfprobs = pd.DataFrame(index=df.index, data=probs)


# In[5]:

print classification_report(df['label'], df['pred'], labels=used_labels, target_names=label_set)


# In[6]:

plt.rcParams['figure.facecolor'] = 'w'


cm = confusion_matrix(df['label'], df['pred'])
cm = cm / cm.sum(axis=1, dtype=float, keepdims=True)  # precision

# show matrix
cmimg = plt.matshow(cm, cmap='Greys', vmin=0, vmax=1)

# show labels
ax = plt.gca()
ax.set_xticklabels([''] + label_set.tolist(), rotation=20, ha='left');
ax.set_yticklabels([''] + label_set.tolist(), ha='right');
ax.tick_params(bottom=False)

# print values in cells
hide_threshold=0.005
annot_fmt = '{0:.2f}'
colorvalues = np.mean(cmimg.to_rgba( cm )[:,:,0:3], axis=-1)
you_have_been_warned = False
for i in xrange(cm.shape[0]):
    for j in xrange(cm.shape[1]):
        if cm[i,j] < hide_threshold:
            continue
        ax.annotate(annot_fmt.format(cm[i,j]), xy=(j, i), 
                    horizontalalignment='center', verticalalignment='center', 
                    color= 'k' if colorvalues[i,j] > .5 else 'w')


# In[ ]:

fig = ax.figure
fig.savefig(os.path.join(base_local, 'confusion_matrix.png'), bbox_inches='tight', dpi=100)
