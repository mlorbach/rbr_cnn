import os, errno

import numpy as np
import pandas as pd
from glob import glob
import sklearn.metrics
from sklearn.utils.multiclass import unique_labels
from sklearn.preprocessing import LabelEncoder

import logging
import ipywidgets as widgets
from matplotlib.figure import Figure
            
            
def classification_report(y_true, y_pred, labels, target_names=None, sample_weight=None, digits=2):

    """Build a text report showing the main classification metrics

    Parameters
    ----------
    y_true : 1d array-like, or label indicator array / sparse matrix
        Ground truth (correct) target values.

    y_pred : 1d array-like, or label indicator array / sparse matrix
        Estimated targets as returned by a classifier.

    labels : array, shape = [n_labels]
        Optional list of label indices to include in the report.

    target_names : list of strings
        Optional display names matching the labels (same order).

    sample_weight : array-like of shape = [n_samples], optional
        Sample weights.

    digits : int
        Number of digits for formatting output floating point values

    Returns
    -------
    report : string
        Text summary of the precision, recall, F1 score for each class.

    Examples
    --------
    >>> from sklearn.metrics import classification_report
    >>> y_true = [0, 1, 2, 2, 2]
    >>> y_pred = [0, 0, 2, 2, 1]
    >>> target_names = ['class 0', 'class 1', 'class 2']
    >>> print(classification_report(y_true, y_pred, target_names=target_names))
                 precision    recall  f1-score   support
    <BLANKLINE>
        class 0       0.50      1.00      0.67         1
        class 1       0.00      0.00      0.00         1
        class 2       1.00      0.67      0.80         3
    <BLANKLINE>
    avg / total       0.70      0.60      0.61         5
    <BLANKLINE>

    """

    if labels is None:
        labels = unique_labels(y_true, y_pred)
    else:
        labels = np.asarray(labels)

    avgs_line_heading = 'avg / total'
    avgc_line_heading = 'avg / class'

    if target_names is None:
        target_names = ['%s' % l for l in labels]
        width = max(len(avgs_line_heading), max(len(str(cn)) for cn in labels))
    else:
        width = max(len(str(cn)) for cn in target_names)
        width = max(width, len(avgs_line_heading), digits)

    # remove underscores from target names
    target_names = [str(t).replace('_', ' ') for t in target_names]

    headers = ["precision", "recall", "f1-score", "support"]
    fmt = '%% %ds' % width  # first column: class name
    fmt += '  '
    fmt += ' '.join(['% 9s' for _ in headers])
    fmt += '\n'

    headers = [""] + headers
    report = fmt % tuple(headers)
    report += '\n'

    p, r, f1, s = precision_recall_fscore_support(y_true, y_pred,
                                                  labels=labels,
                                                  average=None,
                                                  sample_weight=sample_weight)

    for i, label in enumerate(labels):
        values = [target_names[i]]
        for v in (p[i], r[i], f1[i]):
            values += ["{0:0.{1}f}".format(v, digits)]
        values += ["{0}".format(s[i])]
        report += fmt % tuple(values)

    report += '\n'

    # compute averages
    values = [avgs_line_heading]
    for v in (np.average(p, weights=s),
              np.average(r, weights=s),
              np.average(f1, weights=s)):
        values += ["{0:0.{1}f}".format(v, digits)]
    values += ['{0}'.format(np.sum(s))]
    report += fmt % tuple(values)

    # compute class averages
    values = [avgc_line_heading]
    for v in (np.average(p),
              np.average(r),
              np.average(f1)):
        values += ["{0:0.2f}".format(v)]
    values += ['{0}'.format(len(labels))]
    report += fmt % tuple(values)

    return report
    
    
def precision_recall_fscore_support(y_true, y_pred, labels, as_matrix=False, **kwargs):
    '''
    Returns precision_recall_fscore_support that only considers the labels provided in 'labels'.

    Use this function to leave out specific targets in your results (e.g., the 'other' class),
    or samples for which you don't have a label.

    Both true and predicted labels are filtered.

    '''

    valid_targets = np.in1d( y_true, labels )
    valid_targets &= np.in1d( y_pred, labels )

    result = sklearn.metrics.precision_recall_fscore_support(np.compress(valid_targets, y_true), np.compress(valid_targets, y_pred), labels=labels, **kwargs)

    if as_matrix:
        return np.vstack(result).T
    else:
        return result


def get_save_button(fig, filename, dpi=None):
    """

    :param fig:
    :param filename:
    :param dpi:
    :return:
    """

    logger = logging.getLogger(__name__)

    def save_figure(name):
        assert isinstance(fig, Figure)
        if not os.path.isdir(os.path.dirname(filename_widget.value)):
            mkdir_p(os.path.dirname(filename_widget.value))
        fig.savefig(filename_widget.value, bbox_inches='tight', dpi=dpi)
        logger.info('Saved figure to "{}".'.format(os.path.abspath(filename_widget.value)))

    filename_widget = widgets.Text(value=filename)
    btn_save = widgets.Button(description='Save figure')
    btn_save.on_click(save_figure)

    return widgets.HBox(children=[filename_widget, btn_save])


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def label_merge_transform(Y, labels_to_merge, group_label=None):

    n_labels = len(labels_to_merge)

    if n_labels == 0:
        return Y

    if group_label is None:
        if n_labels > 0:
            group_label = labels_to_merge[0]
        else:
            group_label = 'merged'

    if n_labels == 2 and group_label == labels_to_merge[0]:
        Y[Y == labels_to_merge[1]] = group_label
    else:
        mask = np.in1d(Y, labels_to_merge)
        Y[mask] = group_label

    return Y


def load_ground_truth_labels(root_folder='~/datasets/RatSI/annotations',
                             label_col='action', has_header=True,
                             incr_frame_nr=1, return_label_encoder=False, other_is_contact=False):

    filenames = glob(os.path.join(os.path.expanduser(root_folder), '*.csv'))

    print 'Found {} annotation files.'.format(len(filenames))

    df = {}
    for fn in filenames:
        video = os.path.splitext(os.path.basename(fn))[0]

        if has_header:
            df[video] = pd.read_csv(fn, sep=';', index_col=0, header=0)
        else:
            df[video] = pd.read_csv(fn, sep=';', index_col=0, header=None,
                                    names=[label_col])

        df[video].index += incr_frame_nr

    df = pd.concat(df, names=['video', 'frame'])

    to_merge = ['Allogrooming',
               'Nape attacking',
               'Pinning',
               'Social Nose Contact']
    if other_is_contact:
        to_merge.append('Other')

    df[label_col] = label_merge_transform(df[label_col],
                                          to_merge,
                                          'Contact')
    le = LabelEncoder()
    df['label'] = le.fit_transform(df[label_col])

    if label_col != 'label':
        df = df.drop(label_col, axis=1)

    print 'Loaded annotations of {} videos.'.format(len(df.index.get_level_values('video').unique()))

    if return_label_encoder:
        return df, le
    else:
        return df
