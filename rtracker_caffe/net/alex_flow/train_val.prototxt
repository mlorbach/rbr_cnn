name: "Alex+Flow"
layer {
  name: "data"
  type: "Data"
  top: "data"
  top: "label"
  include {
    phase: TRAIN
  }
  transform_param {
    mirror: true
    crop_size: 227
    scale: 0.003921569
    mean_value: 0.45098  # mean gray (115/255) from imagenet and zero-flow
    mean_value: 0.5
    mean_value: 0.5
  }
  data_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data_flow/ratsi_train_lmdb"
    batch_size: 512
    backend: LMDB
  }
}
layer {
  name: "data"
  type: "Data"
  top: "data"
  top: "label"
  include {
    phase: TEST
  }
  transform_param {
    mirror: false
    crop_size: 227
    scale: 0.003921569
    mean_value: 0.45098  # mean gray from imagenet and zero-flow
    mean_value: 0.5
    mean_value: 0.5
  }
  data_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data_flow/ratsi_val_lmdb"
    batch_size: 128
    backend: LMDB
  }
}
layer {
  name: "slice_flow"
  type: "Slice"
  bottom: "data"
  top: "image"
  top: "flow"
  slice_param {
    axis: 1
    slice_point: 1
  }
}

####################################
### AlexNet ########################
####################################

layer {
  name: "conv1"
  type: "Convolution"
  bottom: "image"
  top: "conv1"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    kernel_size: 11
    stride: 4
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu1"
  type: "ReLU"
  bottom: "conv1"
  top: "conv1"
}
layer {
  name: "pool1"
  type: "Pooling"
  bottom: "conv1"
  top: "pool1"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "norm1"
  type: "LRN"
  bottom: "pool1"
  top: "norm1"
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layer {
  name: "conv2"
  type: "Convolution"
  bottom: "norm1"
  top: "conv2"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    pad: 2
    kernel_size: 5
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu2"
  type: "ReLU"
  bottom: "conv2"
  top: "conv2"
}
layer {
  name: "pool2"
  type: "Pooling"
  bottom: "conv2"
  top: "pool2"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "norm2"
  type: "LRN"
  bottom: "pool2"
  top: "norm2"
  lrn_param {
    local_size: 5
    alpha: 0.0001
    beta: 0.75
  }
}
layer {
  name: "conv3"
  type: "Convolution"
  bottom: "norm2"
  top: "conv3"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu3"
  type: "ReLU"
  bottom: "conv3"
  top: "conv3"
}
layer {
  name: "conv4"
  type: "Convolution"
  bottom: "conv3"
  top: "conv4"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 384
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu4"
  type: "ReLU"
  bottom: "conv4"
  top: "conv4"
}
layer {
  name: "conv5"
  type: "Convolution"
  bottom: "conv4"
  top: "conv5"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu5"
  type: "ReLU"
  bottom: "conv5"
  top: "conv5"
}
layer {
  name: "pool5"
  type: "Pooling"
  bottom: "conv5"
  top: "pool5"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}


####################################
### FLOW ###########################
####################################

layer {
  name: "conv1_f"
  type: "Convolution"
  bottom: "flow"
  top: "conv1_f"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    kernel_size: 7
    stride: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu1_f"
  type: "ReLU"
  bottom: "conv1_f"
  top: "conv1_f"
}
layer {
  name: "pool1_f"
  type: "Pooling"
  bottom: "conv1_f"
  top: "pool1_f"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv2_f"
  type: "Convolution"
  bottom: "pool1_f"
  top: "conv2_f"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    pad: 2
    kernel_size: 5
    stride: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu2_f"
  type: "ReLU"
  bottom: "conv2_f"
  top: "conv2_f"
}
layer {
  name: "pool2_f"
  type: "Pooling"
  bottom: "conv2_f"
  top: "pool2_f"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv3_f"
  type: "Convolution"
  bottom: "pool2_f"
  top: "conv3_f"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 96
    pad: 1
    kernel_size: 3
    stride: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu3_f"
  type: "ReLU"
  bottom: "conv3_f"
  top: "conv3_f"
}


####################################
### MERGING APPEARENCE + FLOW ######
####################################
## from alexnet: pool5 (N x 256 x ...)
## from flow: conv3_f (N x 96 x ...)

layer {
  name: "pool5_flat"
  type: "Flatten"
  bottom: "pool5"
  top: "pool5_flat"
}

layer {
  name: "conv3_f_flat"
  type: "Flatten"
  bottom: "conv3_f"
  top: "conv3_f_flat"
}

layer {
  name: "concat_img_flow"
  type: "Concat"
  bottom: "conv3_f_flat"
  bottom: "pool5_flat"
  top: "joint_img_flow"
  concat_param {
    axis: 1
  }
}

layer {
  name: "fc6_j"
  type: "InnerProduct"
  bottom: "joint_img_flow"
  top: "fc6_j"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu6_j"
  type: "ReLU"
  bottom: "fc6_j"
  top: "fc6_j"
}
layer {
  name: "drop6_j"
  type: "Dropout"
  bottom: "fc6_j"
  top: "fc6_j"
  dropout_param {
    dropout_ratio: 0.5
  }
}
layer {
  name: "fc7_j"
  type: "InnerProduct"
  bottom: "fc6_j"
  top: "fc7_j"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 4096
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu7_j"
  type: "ReLU"
  bottom: "fc7_j"
  top: "fc7_j"
}
layer {
  name: "drop7_j"
  type: "Dropout"
  bottom: "fc7_j"
  top: "fc7_j"
  dropout_param {
    dropout_ratio: 0.5
  }
}

layer {
  name: "fc8_j"
  type: "InnerProduct"
  bottom: "fc7_j"
  top: "fc8_j"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 1024
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu8_j"
  type: "ReLU"
  bottom: "fc8_j"
  top: "fc8_j"
}

layer {
  name: "fc9_ratsi"
  type: "InnerProduct"
  bottom: "fc8_j"
  top: "fc9_ratsi"
  param {
    lr_mult: 10
    decay_mult: 1
  }
  param {
    lr_mult: 20
    decay_mult: 0
  }
  inner_product_param {
    num_output: 7
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "accuracy"
  type: "Accuracy"
  bottom: "fc9_ratsi"
  bottom: "label"
  top: "accuracy"
  include {
    phase: TEST
  }
  accuracy_param {
    ignore_label: 6
  }
}
layer {
  name: "prob"
  type: "Softmax"
  bottom: "fc9_ratsi"
  top: "prob"
}
layer {
  name: "loss"
  type: "InfogainLoss"
  bottom: "prob"
  bottom: "label"
  top: "loss"
  infogain_loss_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data/ratsi_infogain.binaryproto"
  }
}
