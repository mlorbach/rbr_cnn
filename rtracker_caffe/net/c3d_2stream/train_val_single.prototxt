name: "C3D_RatSI_twostream"

##################
# TRAINING INPUT #
##################
layer {
  name: "data_s0"
  type: "VideoData"
  top: "data_s0"
  top: "label_s0"
  include {
    phase: TRAIN
  }
  transform_param {
    #mean_value: 53  # mean of patches
    mirror: false
  }
  video_data_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data_2stream/train.txt"
    root_folder: "/home/malte/datasets/RatSI/box_frames/subject_0/"
    new_length: 5
    sampling_rate: 2
    use_image: true
    show_data: false
    use_temporal_jitter: false
    batch_size: 30
    shuffle: false  # are already shuffled in list
  }
}
layer {
    name: "data_tracking"
    type: "HDF5Data"
    top: "feats_tracking"
    top: "label"
    include {
        phase: TRAIN
    }
    hdf5_data_param {
        source: "/home/malte/rbr_cnn/rtracker_caffe/data_2stream/train_tracking_features.lst"
        batch_size: 30
        shuffle: false
    }
}

####################
# VALIDATION INPUT #
####################

layer {
  name: "data_s0"
  type: "VideoData"
  top: "data_s0"
  top: "label_s0"
  include {
    phase: TEST
  }
  transform_param {
    #mean_value: 53  # mean of patches
    mirror: false
  }
  video_data_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data_2stream/val.txt"
    root_folder: "/home/malte/datasets/RatSI/box_frames/subject_0/"
    new_length: 5
    sampling_rate: 2
    use_image: true
    show_data: false
    use_temporal_jitter: false
    batch_size: 30
    shuffle: false  # are already shuffled in list
  }
}

layer {
    name: "data_tracking"
    type: "HDF5Data"
    top: "feats_tracking"
    top: "label"
    include {
        phase: TEST
    }
    hdf5_data_param {
        source: "/home/malte/rbr_cnn/rtracker_caffe/data_2stream/val_tracking_features.lst"
        batch_size: 30
        shuffle: false
    }
}


layer {
    name: "silence_s0"
    type: "Silence"
    bottom: "label_s0"
}



##################################################################
# NET SUBJECT 0 #
##################################################################

# ----- 1st group -----
layer {
  name: "conv1_a"
  type: "Convolution3D"
  bottom: "data_s0"
  top: "conv1_a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution3d_param {
    num_output: 64
    kernel_size: 3
    kernel_depth: 3
    stride: 1
    temporal_stride: 1
    pad: 1
    temporal_pad: 1
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu1_a"
  type: "ReLU"
  relu_param {
    engine: CAFFE
  }
  bottom: "conv1_a"
  top: "conv1_a"
}
layer {
  name: "pool1_a"
  type: "Pooling3D"
  bottom: "conv1_a"
  top: "pool1_a"
  pooling3d_param {
    pool: MAX
    kernel_size: 2
    kernel_depth: 1
    stride: 2
    temporal_stride: 1
  }
}

# ----- 2nd group -----
layer {
  name: "conv2_a"
  type: "Convolution3D"
  bottom: "pool1_a"
  top: "conv2_a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution3d_param {
    num_output: 128
    kernel_size:3
    kernel_depth: 3
    stride: 1
    temporal_stride: 1
    pad: 1
    temporal_pad: 1
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu2_a"
  type: "ReLU"
  relu_param {
    engine: CAFFE
  }
  bottom: "conv2_a"
  top: "conv2_a"
}
layer {
  name: "pool2_a"
  type: "Pooling3D"
  bottom: "conv2_a"
  top: "pool2_a"
  pooling3d_param {
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }
}

# ----- 3rd group -----
layer {
  name: "conv3_a"
  type: "Convolution3D"
  bottom: "pool2_a"
  top: "conv3_a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution3d_param {
    num_output: 256
    kernel_size: 3
    kernel_depth: 3
    stride: 1
    temporal_stride: 1
    pad: 1
    temporal_pad: 1
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu3_a"
  type: "ReLU"
  relu_param {
    engine: CAFFE
  }
  bottom: "conv3_a"
  top: "conv3_a"
}
layer {
  name: "pool3_a"
  type: "Pooling3D"
  bottom: "conv3_a"
  top: "pool3_a"
  pooling3d_param {
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }
}

# ----- 4th group -----
layer {
  name: "conv4_a"
  type: "Convolution3D"
  bottom: "pool3_a"
  top: "conv4_a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution3d_param {
    num_output: 256
    kernel_size: 3
    kernel_depth: 3
    stride: 1
    temporal_stride: 1
    pad: 1
    temporal_pad: 1    
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu4_a"
  type: "ReLU"
  relu_param {
    engine: CAFFE
  }
  bottom: "conv4_a"
  top: "conv4_a"
}
layer {
  name: "pool4_a"
  type: "Pooling3D"
  bottom: "conv4_a"
  top: "pool4_a"
  pooling3d_param {
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }
}

# ----- 5th group -----
layer {
  name: "conv5_a"
  type: "Convolution3D"
  bottom: "pool4_a"
  top: "conv5_a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution3d_param {
    num_output: 256
    kernel_size: 3
    kernel_depth: 3
    stride: 1
    temporal_stride: 1
    pad: 1
    temporal_pad: 1
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu5_a"
  type: "ReLU"
  relu_param {
    engine: CAFFE
  }
  bottom: "conv5_a"
  top: "conv5_a"
}
layer {
  name: "pool5_a"
  type: "Pooling3D"
  bottom: "conv5_a"
  top: "pool5_a"
  pooling3d_param {
    pool: MAX
    kernel_size: 2
    kernel_depth: 2
    stride: 2
    temporal_stride: 2
  }
}

# ----- 1st fc group -----
layer {
  name: "fc6_a"
  type: "InnerProduct"
  bottom: "pool5_a"
  top: "fc6_a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 1024
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu6_a"
  type: "ReLU"
  relu_param {
    engine: CAFFE
  }
  bottom: "fc6_a"
  top: "fc6_a"
}



##################################################################
# MERGING STREAMS #
##################################################################


layer {
  name: "drop6_merge"
  type: "Dropout"
  bottom: "fc6_a"
  top: "fc6_a"
  dropout_param {
    dropout_ratio: 0.5
  }
}

# ----- 2nd fc group -----
layer {
  name: "fc7"
  type: "InnerProduct"
  bottom: "fc6_a"
  top: "fc7"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 1024
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu7"
  type: "ReLU"
  relu_param {
    engine: CAFFE
  }
  bottom: "fc7"
  top: "fc7"
}
layer {
  name: "drop7"
  type: "Dropout"
  bottom: "fc7"
  top: "fc7"
  dropout_param {
    dropout_ratio: 0.5
  }
}


# merge tracking features
layer {
    name: "concat_track_motion"
    type: "Concat"
    bottom: "fc7"
    bottom: "feats_tracking"
    top: "fc7_merge"
    concat_param {
        axis: 1
    }
}

# ----- 3rd fc group -----
layer {
  name: "fc8"
  type: "InnerProduct"
  bottom: "fc7_merge"
  top: "fc8"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 7
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}


##################################################################
# OUTPUT #
##################################################################

#layer {
#    name: "debug"
#    type: "Split"
#    bottom: "label"
#    top: "label_"
#    top: "label_debug"
#}

layer {
  name: "accuracy"
  type: "Accuracy"
  bottom: "fc8"
  bottom: "label"
  top: "accuracy"
  include {
    phase: TEST
  }
}
layer {
  name: "loss"
  type: "SoftmaxWithLoss"
  bottom: "fc8"
  bottom: "label"
  top: "loss"
  loss_param {
    ignore_label: 6
  }
}
#layer {
#  name: "loss"
#  type: "InfogainLoss"
#  bottom: "prob"
#  bottom: "label_"
#  top: "loss"
#  infogain_loss_param {
#    source: "/home/malte/rbr_cnn/rtracker_caffe/data_2stream/ratsi_infogain.binaryproto"
#  }
#}