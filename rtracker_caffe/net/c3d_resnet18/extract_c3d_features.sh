#!/usr/bin/env sh
set -e

MODELROOT=examples/c3d_ucf101_feature_extraction
NETROOT=/home/malte/rbr_cnn/rtracker_caffe/net/c3d_resnet18

# parameter
#  1: network def (.prototxt)
#  2: weights file (.caffemodel)
#  3: gpu device id
#  4: batch size
#  5: n mini batches
#  6: path to file with videos
#  7: features to extract [feat1 [feat2 [...]]]  (== blob name in network)

GLOG_logtostderr=1 ./build/tools/extract_image_features \
    ${NETROOT}/c3d_resnet18_ucf101_feature_extraction.prototxt \
    ${MODELROOT}/c3d_resnet18_sports1m_r2_iter_2800000.caffemodel \
    0 \
    30 50000 \
    ${NETROOT}/features_output.prefix \
    pool5 res5b