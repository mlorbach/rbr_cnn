#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
    if [ $1="nogpu" ]; then
        GPU=""
        shift  # consume arg1, shift so that we can use $@ below
    fi
fi

MODELROOT=examples/c3d_ucf101_feature_extraction
NETROOT=/home/malte/rbr_cnn/rtracker_caffe/net/c3d_resnet18

# parameter
#  1: network def (.prototxt)
#  2: weights file (.caffemodel)
#  3: gpu device id
#  4: batch size
#  5: n mini batches
#  6: path to file with videos
#  7: features to extract [feat1 [feat2 [...]]]  (== blob name in network)

# GLOG_logtostderr=1 ./build/tools/extract_image_features \
#     ${NETROOT}/c3d_resnet18_ucf101_feature_extraction.prototxt \
#     ${MODELROOT}/c3d_resnet18_sports1m_r2_iter_2800000.caffemodel \
#     0 \
#     30 50000 \
#     ${NETROOT}/features_output.prefix \
#     pool5 res5b

GLOG_logtostderr=1 ./build/tools/caffe train \
    --solver=${NETROOT}/solver_c3d_resn18_finetuning_ratsi.prototxt \
    --weights=${MODELROOT}/c3d_resnet18_sports1m_r2_iter_2800000.caffemodel \
    $GPU \
    $@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi.log


# old caffe v1.0 model: ../c3d_feature_extraction/conv3d_deepnetA_sport1m_iter_1900000
