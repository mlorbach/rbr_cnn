#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

MODELITER=14000
NUMITER=182  # = 9089 (test files) / 50 (batch size)
GLOG_logtostderr=1 ./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/c3d_resnet18/c3d_resn18_ratsi_test.prototxt \
	--weights=/home/malte/rbr_cnn/rtracker_caffe/models/c3d_resnet18_finetune_ratsi_iter_${MODELITER}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
