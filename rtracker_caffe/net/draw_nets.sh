#!/bin/bash
# Draws all net architectures it can find in the current directory as PNG images. It looks for Caffe's *.prototxt files, but skips files that start with "solver*" or "deploy*".

echo "Activate Python environment"
source activate rbr_cnn

# find all *.prototxt files except solver* and deploy*, and executes the draw_net.py script provided by Caffe.
find . -maxdepth 2 -name "*.prototxt" -type f -not -name "solver*" -not -name "deploy*" -exec python /home/malte/Install/caffe_rbr/python/draw_net.py --rankdir BT --phase TRAIN {} {}.png \;
#find . -maxdepth 2 -name "*.prototxt" -type f -not -name "solver*" -not -name "deploy*" -exec python /home/malte/Install/C3D/C3D-v1.1/python/draw_net.py --rankdir BT --phase TRAIN {} {}.png \;

echo "Deactivate Python environment"
source deactivate
echo "Finished."