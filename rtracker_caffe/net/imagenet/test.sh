#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

# --weights=/home/malte/Documents/Results/2017-02-24_Imagenet_finetune_all_cweight/imagenet_finetune_iter_5000.caffemodel \

NUMITER=91  # 45457 (images in test) / 500 (test batch size)
#ITERATION=1940
./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/imagenet/test.prototxt \
    --weights=/home/malte/rbr_cnn/rtracker_caffe/models/imagenet_finetune_iter_${ITERATION}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
