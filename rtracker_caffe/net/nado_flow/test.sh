#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

MODELITER=10000
NUMITER=100
./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/nado_flow/test.prototxt \
	--weights=/home/malte/rbr_cnn/rtracker_caffe/models/nado_flow_iter_${MODELITER}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
