#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

MODELITER=1620
# test has 9089 samples / batch 456 = 91
# 15145 / 456 = 33.2
NUMITER=34
./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/track_feats_only/test.prototxt \
	--weights=/home/malte/rbr_cnn/rtracker_caffe/models/track_feats_only_iter_${MODELITER}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
