#!/usr/bin/env sh
set -e

GPU="-gpu 0"
MODELITER=8290
if [ $# -gt 0 ]; then
    if [ "$1" = "nogpu" ]; then
        GPU=""
        shift  # consume arg1, shift so that we can use $@ below
    else
        MODELITER=$1
        shift
    fi
fi

if [ $# -gt 0 ]; then
    if [ "$1" = "nogpu" ]; then
        GPU=""
        shift  # consume arg1, shift so that we can use $@ below
    else
        MODELITER=$1
        shift
    fi
fi

# test (10|1 s5) has 9088 samples / batch 100 = 91
NUMITER=91
./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/vgg-16-flow/test_reduced.prototxt \
	--weights=/home/malte/rbr_cnn/rtracker_caffe/models/vgg_16_flow_iter_${MODELITER}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
