From: https://github.com/BVLC/caffe/wiki/Model-Zoo#models-from-the-bmvc-2014-paper-return-of-the-devil-in-the-details-delving-deep-into-convolutional-nets

Original Project: "Return of the Devil in the Details: Delving Deep into Convolutional Nets" (http://www.robots.ox.ac.uk/%7Evgg/research/deep_eval)

Model files: https://gist.github.com/ksimonyan/78047f3591446d1d7b91#file-readme-md