#!/usr/bin/env sh
set -e

if [ $# -gt 0 ]; then
    SPLIT=$1
fi

CAFFE_ROOT=~/Install/caffe_rbr

MODEL=/home/malte/rbr_cnn/rtracker_caffe/net/vgg-m-2048/extract_${SPLIT}.prototxt
WEIGHTS=/home/malte/Documents/Results/2017-03-15_VGG-M/exp04/vgg-m-2048_iter_2584.caffemodel
LAYER="conv5,label"
OUTPUT="/home/malte/Documents/Results/2017-03-15_VGG-M/exp04/features/${SPLIT},/home/malte/Documents/Results/2017-03-15_VGG-M/exp04/features/${SPLIT}_label"

# create output dir if it doesn't exist yet
if [ -d "$OUTPUT" ]; then
    echo "Output dir: ${OUTPUT}"
    echo "ERROR: output directory already exists which is not allowed for LMDB output. Remove and try again."
    exit
fi


# pick a good batch size, minibatch must be the same as in .prototxt
MINIBATCH=500

case $SPLIT in
    train) 
        DATASIZE=20716 ;;
    test)
        DATASIZE=15145 ;;
    val)
        DATASIZE=10589 ;;
esac

BATCHSIZE=$(( ($DATASIZE + $MINIBATCH - 1) / $MINIBATCH ))
echo "Extract from ${SPLIT} using ${BATCHSIZE} batches."

 
${CAFFE_ROOT}/build/tools/extract_features ${WEIGHTS} ${MODEL} ${LAYER} ${OUTPUT} ${BATCHSIZE} lmdb GPU