#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

GLOG_log_dir=/home/malte/rbr_cnn/rtracker_caffe/log ./build/tools/caffe train \
	--solver=/home/malte/rbr_cnn/rtracker_caffe/net/vgg-m-2048/solver_crim.prototxt \
	--weights=/home/malte/rbr_cnn/rtracker_caffe/net/vgg-m-2048/VGG_CNN_M_2048.caffemodel \
	$GPU