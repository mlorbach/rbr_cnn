The motion CNN model (trained on UCF-101 split-1) can be downloaded using the following links:

http://bigvid.fudan.edu.cn/model/two_stream_temporal_sp1.caffemodel
http://bigvid.fudan.edu.cn/model/two_stream_temporal_deploy.prototxt
http://bigvid.fudan.edu.cn/model/ucf_sof10L_mean_sp1.binaryproto
