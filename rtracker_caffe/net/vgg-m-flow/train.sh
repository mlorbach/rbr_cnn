#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

./build/tools/caffe train \
	--solver=/home/malte/rbr_cnn/rtracker_caffe/net/vgg-m-flow/solver.prototxt \
    --weights=/home/malte/rbr_cnn/rtracker_caffe/net/vgg-m-flow/two_stream_temporal_sp1_v1.caffemodel \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi.log
