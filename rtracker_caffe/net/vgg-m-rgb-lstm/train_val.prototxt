name: "VGG-M-RGB_LSTM"

##################
# TRAINING INPUT #
##################
layer {
  name: "data"
  type: "VideoData"
  top: "data_vol"
  top: "label"
  top: "clip_marker"
  video_data_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data/data_flow_norm_rep/train.txt"
    root_folder: "/home/malte/datasets/RatSI/video_frames/"
    batch_size: 25
    new_length: 16
    sampling_rate: 1
    modality: "image"
    shuffle: true
    show_data: false
  }
  transform_param{
    crop_size: 224
    mirror: false
    rotate: true
    mean_value: 40
  }
  include: { phase: TRAIN }
}

####################
# VALIDATION INPUT #
####################

layer {
  name: "data"
  type: "VideoData"
  top: "data_vol"
  top: "label"
  top: "clip_marker"
  video_data_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data/data_flow_norm_rep/val.txt"
    root_folder: "/home/malte/datasets/RatSI/video_frames/"
    batch_size: 15
    new_length: 16
    modality: "image"
    sampling_rate: 1
    shuffle: true
  }
  transform_param{
    crop_size: 224
    mirror: false
    rotate: true
    mean_value: 40
    #scale: 0.15625  # 20 / 128 (flow is clipped at +- 20)
  }
  include: { phase: TEST }
}

# Permute dimensions of ImageSequence input (swap clip length with channels)
#  Bot shape: -> N x C x T x H x W
#  Top shape: -> T x N x C x H x W
layer {
  name: "permute_img_sequence"
  type: "Permute"
  bottom: "data_vol"
  top: "data_perm"
  permute_param {
    order: 2
    order: 0
    order: 1
    order: 3
    order: 4
  }
}

# Reshape to T*N x C x H x W
#  (fake as if batch size was T*N)
layer {
  name: "reshape_img_sequence"
  type: "Reshape"
  bottom: "data_perm"
  top: "data"
  reshape_param {
    shape {
      dim: -1
      dim: 3
      dim: 224
      dim: 224
    }
  }
}


layer {
  name: "conv1"
  type: "Convolution"
  bottom: "data"
  top: "conv1"
  convolution_param {
    num_output: 96
    kernel_size: 7
    stride: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
}
layer {
  name: "relu1"
  type: "ReLU"
  bottom: "conv1"
  top: "conv1"
}
layer {
  name: "norm1"
  type: "LRN"
  bottom: "conv1"
  top: "norm1"
  lrn_param {
    local_size: 5
    alpha: 0.0005
    beta: 0.75
    k: 2
  }
}
layer {
  name: "pool1"
  type: "Pooling"
  bottom: "norm1"
  top: "pool1"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv2"
  type: "Convolution"
  bottom: "pool1"
  top: "conv2"
  convolution_param {
    num_output: 256
    pad: 1
    kernel_size: 5
    stride: 2
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
}
layer {
  name: "relu2"
  type: "ReLU"
  bottom: "conv2"
  top: "conv2"
}
layer {
  name: "norm2"
  type: "LRN"
  bottom: "conv2"
  top: "norm2"
  lrn_param {
    local_size: 5
    alpha: 0.0005
    beta: 0.75
    k: 2
  }
}
layer {
  name: "pool2"
  type: "Pooling"
  bottom: "norm2"
  top: "pool2"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "conv3"
  type: "Convolution"
  bottom: "pool2"
  top: "conv3"
  convolution_param {
    num_output: 512
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
}
layer {
  name: "relu3"
  type: "ReLU"
  bottom: "conv3"
  top: "conv3"
}
layer {
  name: "conv4"
  type: "Convolution"
  bottom: "conv3"
  top: "conv4"
  convolution_param {
    num_output: 512
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
  param {
    lr_mult: 0
    decay_mult: 0
  }
}
layer {
  name: "relu4"
  type: "ReLU"
  bottom: "conv4"
  top: "conv4"
}
layer {
  name: "conv5_ratsi"
  type: "Convolution"
  bottom: "conv4"
  top: "conv5"
  convolution_param {
    num_output: 256
    pad: 1
    kernel_size: 3
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
}
layer {
  name: "relu5"
  type: "ReLU"
  bottom: "conv5"
  top: "conv5"
}
layer {
  name: "pool5"
  type: "Pooling"
  bottom: "conv5"
  top: "pool5"
  pooling_param {
    pool: MAX
    kernel_size: 3
    stride: 2
  }
}
layer {
  name: "fc6_ratsi"
  type: "InnerProduct"
  bottom: "pool5"
  top: "fc6"
  inner_product_param {
    num_output: 2048
    weight_filler {
      type: "gaussian"
      std: 0.005
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
}
layer {
  name: "relu6"
  type: "ReLU"
  bottom: "fc6"
  top: "fc6"
}
layer {
  name: "drop6"
  type: "Dropout"
  bottom: "fc6"
  top: "fc6"
  dropout_param {
    dropout_ratio: 0.5
  }
}

# fc6 top: T*N x 2048
# Reshape to: T x N x 2048
layer{
  name: "reshape-fc"
  type: "Reshape"
  bottom: "fc6"
  top: "fc6-reshape"
  reshape_param{
    shape{
      dim: 16
      dim: -1
      dim: 2048
    }
  }
}

layer {
  name: "repeat-label"
  type: "Tile"
  bottom: "label"
  top: "label-repeat"
  tile_param {
    axis: 0
    tiles: 16
  }
}

layer {
  name: "reshape-label"
  type: "Reshape"
  bottom: "label-repeat"
  top: "label-reshape"
  reshape_param {
    shape {
      dim: 16
      dim: -1
    }
  }
}

########################################################################
# LSTM
########################################################################


layer {
  name: "lstm1"
  type: "LSTM"
  bottom: "fc6-reshape"
  bottom: "clip_marker"
  top: "lstm1"
  recurrent_param {
    num_output: 256
    weight_filler {
      type: "uniform"
      min: -0.01
      max: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
}
layer {
  name: "lstm1-drop"
  type: "Dropout"
  bottom: "lstm1"
  top: "lstm1-drop"
  dropout_param {
    dropout_ratio: 0.5
  }
}
layer {
  name: "fc8-final"
  type: "InnerProduct"
  bottom: "lstm1-drop"
  top: "fc8-final"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  inner_product_param {
    num_output: 6
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
    axis: 2
  }
}


########################################################################
# OUTPUT
########################################################################

layer {
  name: "accuracy"
  type: "Accuracy"
  bottom: "fc8-final"
  bottom: "label-reshape"
  top: "accuracy"
  include {
    phase: TEST
  }
  accuracy_param {
    axis: 2
    ignore_label: 5
  }
}

layer {
  name: "loss"
  type: "SoftmaxWithLoss"
  bottom: "fc8-final"
  bottom: "label-reshape"
  top: "loss"
  loss_param {
    ignore_label: 5
  }
  softmax_param {
    axis: 2
  }
}

#layer {
#  name: "loss"
#  type: "InfogainLoss"
#  bottom: "fc8-final"
#  bottom: "label-reshape"
#  top: "loss"
#  infogain_loss_param {
#    source: "/home/malte/rbr_cnn/rtracker_caffe/data/data_flow_norm/ratsi_infogain.binaryproto"
#    axis: 2
#  }
#}