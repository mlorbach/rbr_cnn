#!/usr/bin/env sh
set -e

GPU="-gpu 0"
ITERATION=1940
if [ $# -gt 0 ]; then
	if [ "$1" = "nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
    else
        ITERATION=$1
        shift
	fi
fi

if [ $# -gt 0 ]; then
    if [ "$1" = "nogpu" ]; then
        GPU=""
        shift  # consume arg1, shift so that we can use $@ below
    else
        ITERATION=$1
        shift
    fi
fi

#

NUMITER=16  # 15145 (images in test) / 1000 (test batch size)
./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/vgg_vidcaf_2stream/test_spatial_fusion.prototxt \
    --weights=/home/malte/rbr_cnn/rtracker_caffe/models/vgg_vidcaf_2stream_iter_${ITERATION}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
