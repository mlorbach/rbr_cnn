#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

./build/tools/caffe train \
	--solver=/home/malte/rbr_cnn/rtracker_caffe/net/vgg_vidcaf_2stream/solver.prototxt \
    --weights=/home/malte/Documents/Results/2017-03-13_VidCaf_motion/exp13/vidcaf_motion_iter_3316.caffemodel \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi.log
