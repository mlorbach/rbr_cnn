#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

MODELITER=14850
# test has 9089 samples / batch 100 = 91
NUMITER=91
./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/vidcaf_2stream/test.prototxt \
	--weights=/home/malte/rbr_cnn/rtracker_caffe/models/vidcaf_2stream_iter_${MODELITER}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
