#!/usr/bin/env sh
set -e

if [ $# -gt 0 ]; then
    SPLIT=$1
fi

CAFFE_ROOT=~/Install/caffe_rbr

MODEL=/home/malte/rbr_cnn/rtracker_caffe/net/vidcaf_motion/extract/extract_reduced_${SPLIT}.prototxt
WEIGHTS=/home/malte/Documents/Results/2017-03-13_VidCaf_motion/exp14/256/vidcaf_motion_reduced_iter_4974.caffemodel
LAYER=fc7
OUTPUT=/home/malte/Documents/Results/2017-03-13_VidCaf_motion/exp14/256/features/${SPLIT}

# create output dir if it doesn't exist yet
if [ -d "$OUTPUT" ]; then
    echo "Output dir: ${OUTPUT}"
    echo "ERROR: output directory already exists which is not allowed for LMDB output. Remove and try again."
    exit
fi

# pick a good batch size
MINIBATCH=100

case $SPLIT in
    train) 
        DATASIZE=20716 ;;
    test)
        DATASIZE=15145 ;;
    val)
        DATASIZE=10589 ;;
esac

BATCHSIZE=$(( ($DATASIZE + $MINIBATCH - 1) / $MINIBATCH ))
echo "Extract from ${SPLIT} using ${BATCHSIZE} batches."

${CAFFE_ROOT}/build/tools/extract_features ${WEIGHTS} ${MODEL} ${LAYER} ${OUTPUT} ${BATCHSIZE} lmdb GPU