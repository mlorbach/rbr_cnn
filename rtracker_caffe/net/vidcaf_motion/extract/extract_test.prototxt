name: "vidcaf_motion"

##################
# INPUT #
##################
layer {
  name: "data"
  type: "VideoData"
  top: "data"
  top: "label"
  video_data_param {
    source: "/home/malte/rbr_cnn/rtracker_caffe/data_motion/test.txt"
    root_folder: "/home/malte/datasets/RatSI/video_frames_128x128/"

    # 50 train, 30 val --> ~9Gb GPU memory
    batch_size: 100
    new_length: 12
    use_image: true
    sampling_rate: 1
    shuffle: false
    is_color: true
  }
  transform_param {
    crop_size: 112
    mirror: true
    mean_value: 40
  }
}

##################################################################
# NET 
##################################################################

# ----- 1st group -----
layer {
  name: "conv1a"
  type: "Convolution"
  bottom: "data"
  top: "conv1a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 64
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    pad: 1
    pad: 1
    pad: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}
layer {
  name: "relu1a"
  type: "ReLU"
  bottom: "conv1a"
  top: "conv1a"
}
layer {
  name: "pool1"
  type: "Pooling"
  bottom: "conv1a"
  top: "pool1"
  pooling_param {
    pool: MAX
    kernel_size: 1
    kernel_size: 2
    kernel_size: 2
    stride: 1
    stride: 2
    stride: 2
  }
}

# ----- 2nd group -----
layer {
  name: "conv2a"
  type: "Convolution"
  bottom: "pool1"
  top: "conv2a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 128
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    pad: 1
    pad: 1
    pad: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu2a"
  type: "ReLU"
  bottom: "conv2a"
  top: "conv2a"
}
layer {
  name: "pool2"
  type: "Pooling"
  bottom: "conv2a"
  top: "pool2"
  pooling_param {
    pool: MAX
    kernel_size: 2
    kernel_size: 2
    kernel_size: 2
    stride: 2
    stride: 2
    stride: 2
  }
}

# ----- 3rd group -----
layer {
  name: "conv3a_ratsi"
  type: "Convolution"
  bottom: "pool2"
  top: "conv3a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    pad: 1
    pad: 1
    pad: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu3a"
  type: "ReLU"
  bottom: "conv3a"
  top: "conv3a"
}
layer {
  name: "pool3"
  type: "Pooling"
  bottom: "conv3a"
  top: "pool3"
  pooling_param {
    pool: MAX
    kernel_size: 2
    kernel_size: 2
    kernel_size: 2
    stride: 2
    stride: 2
    stride: 2
  }
}

# ----- 4th group -----
layer {
  name: "conv4a_ratsi"
  type: "Convolution"
  bottom: "pool3"
  top: "conv4a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    pad: 1
    pad: 1
    pad: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu4a"
  type: "ReLU"
  bottom: "conv4a"
  top: "conv4a"
}
layer {
  name: "pool4"
  type: "Pooling"
  bottom: "conv4a"
  top: "pool4"
  pooling_param {
    pool: MAX
    kernel_size: 1
    kernel_size: 2
    kernel_size: 2
    stride: 1
    stride: 1
    stride: 1
  }
}

# ----- 5th group -----
layer {
  name: "conv5a_ratsi"
  type: "Convolution"
  bottom: "pool4"
  top: "conv5a"
  param {
    lr_mult: 1
    decay_mult: 1
  }
  param {
    lr_mult: 2
    decay_mult: 0
  }
  convolution_param {
    num_output: 256
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    pad: 0
    pad: 1
    pad: 1
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 1
    }
  }
}
layer {
  name: "relu5a"
  type: "ReLU"
  bottom: "conv5a"
  top: "conv5a"
}
# 50 256 1 13 13

layer {
  # squeeze out singelton dimension
  name: "reshape_relu5a"
  type: "Reshape"
  bottom: "conv5a"
  top: "conv5a_squeezed"
  reshape_param {
    shape {
      dim: 0
      dim: 0
    }
    axis: 0
    num_axes: 3
  }
}