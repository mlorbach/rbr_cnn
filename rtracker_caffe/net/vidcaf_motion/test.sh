#!/usr/bin/env sh
set -e

GPU="-gpu 0"
MODELITER=8290
if [ $# -gt 0 ]; then
    if [ "$1" = "nogpu" ]; then
        GPU=""
        shift  # consume arg1, shift so that we can use $@ below
    else
        MODELITER=$1
        shift
    fi
fi

if [ $# -gt 0 ]; then
    if [ "$1" = "nogpu" ]; then
        GPU=""
        shift  # consume arg1, shift so that we can use $@ below
    else
        MODELITER=$1
        shift
    fi
fi

# test has 16|1 has 5679 samples / batch 80 = 71
# test set 12|2 has 7573 / 80 = 95
# test set 12|1 s3 has 15145 / 100 = 152
NUMITER=152
./build/tools/caffe test \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/vidcaf_motion/test_reduced_balanced.prototxt \
	--weights=/home/malte/rbr_cnn/rtracker_caffe/models/vidcaf_motion_redbal_iter_${MODELITER}.caffemodel \
	-iterations $NUMITER \
	$GPU \
	$@ 2>&1 | tee -a /home/malte/rbr_cnn/rtracker_caffe/log/ratsi_test.log
