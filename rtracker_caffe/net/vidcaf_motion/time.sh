#!/usr/bin/env sh
set -e

GPU="-gpu 0"
if [ $# -gt 0 ]; then
	if [ $1="nogpu" ]; then
		GPU=""
		shift  # consume arg1, shift so that we can use $@ below
	fi
fi

NUMITER=50
./build/tools/caffe time \
    --model=/home/malte/rbr_cnn/rtracker_caffe/net/vidcaf_motion/train_val_transfer.prototxt \
    -iterations $NUMITER \
    ${GPU}
