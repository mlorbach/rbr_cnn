import os
import caffe
import numpy as np
import argparse


def _load(counts_file):
    assert os.path.exists(counts_file), 'Could not find count file: {}'.format(counts_file)
    counts = np.loadtxt(counts_file, dtype=np.int)
    return counts


def _compute(counts, ignore_label=None, neutral_label=None, use_sklearn=True):
    '''
    Compute class weights given the number of samples in the set.


    '''

    counts = np.asarray(counts)

    if ignore_label is None:
        ignore_label = []
    elif not isinstance(ignore_label, (list, tuple, np.ndarray)):
        ignore_label = [ignore_label]

    if neutral_label is None:
        neutral_label = []
    elif not isinstance(neutral_label, (list, tuple, np.ndarray)):
        neutral_label = [neutral_label]

    print "Ignoring label:", ignore_label
    print "Neutral label:", neutral_label

    mask = counts > 0  # must have at least one sample
    if len(ignore_label) > 0:
        mask[ignore_label] = False  # ignore

    if len(neutral_label) > 0:
        mask[neutral_label] = False  # ignore in max count

    # sklearn implementation:
    #  recip_freq = len(y) / (len(le.classes_) * bincount(y_ind).astype(np.float64))
    #  weight = recip_freq[le.transform(classes)]

    if use_sklearn:
        # total number of samples, without ignored and neutral
        N = np.sum(counts[mask], dtype=float)

        # total number of classes, without ignored and neutral
        K = len(counts) - len(ignore_label) - len(neutral_label)

        neutral_n = np.median(counts[mask])

        print 'N:', N
        print 'K:', K
        print 'neutral n:', neutral_n

        n = counts.astype('f4')
        n[counts == 0] = neutral_n
        n[neutral_label] = neutral_n

        H = np.diag(N / (K * n))

    else:

        # smallest non-zero class
        minY = np.min(counts[mask])

        # if Y was equally distributed, every label occurred this often
        neutral_n = np.sum(counts) / np.sum(mask, dtype=float)

        # set empty classes to neutral
        counts[counts == 0] = neutral_n
        counts[neutral_label] = neutral_n

        # H is minY / |y_i| on diagonal, 0 otherwise
        H = np.diag(minY / counts.astype(float)).astype('f4')

    # no loss for ignored labels, set to 0
    if len(ignore_label):
        H[ignore_label, ignore_label] = 0

    np.set_printoptions(precision=5)
    print H

    return H


def _export_binaryproto(output_file, H):
    assert H.ndim == 2 and (H.shape[0] == H.shape[1]), 'H must be square matrix but is {}'.format(H.shape)

    L = H.shape[0]

    # EXPORT
    blob = caffe.io.array_to_blobproto(H.reshape((1, 1, L, L)))
    with open(output_file, 'wb') as fp:
        fp.write(blob.SerializeToString())

    print 'InfoGain matrix written to {}'.format(output_file)


def compute_class_weights(counts_file, output_file, ignore_label=None, neutral_label=None):
    print 'Load data ...'
    counts = _load(counts_file)
    print 'Compute infogain ...'
    H = _compute(counts, ignore_label, neutral_label)
    print 'Export infogain ...'
    _export_binaryproto(output_file, H)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('counts_file', type=str, help='Text file listing the class counts (1 per line). Order must match labels file.')
    parser.add_argument('output_file', type=str, help='Output file for binary proto matrix.')
    parser.add_argument('-b', '--ignore', type=int, nargs='+', help='Integer label for classes to ignore during training.')
    parser.add_argument('-n', '--neutral', type=int, nargs='+', help='Integer label for classes to assign neutral weight.')
    args = parser.parse_args()

    compute_class_weights(args.counts_file, args.output_file, ignore_label=args.ignore, neutral_label=args.neutral)

    print 'Done.'
