#!/usr/bin/env sh
# This script uses tracking information to create image patches from the bounding box around the tracked points.
# All videos (video_frames) in the input directory are processed in one batch.
set -e

# INPUT directory
VIDEOS=~/datasets/RatSI/video_frames_576x576/Observation

# OUTPUT directory; a subfolder is created for every video file and subject
OUTPUT=~/datasets/RatSI/flow_norm/Observation

CMD=~/rbr_cnn/opticalflow/build/ComputeFlow

#######################################################################

echo "Compute normalized flow for all RatSI videos ..."

$CMD -I ${VIDEOS}01 -O ${OUTPUT}01 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}02 -O ${OUTPUT}02 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}04 -O ${OUTPUT}04 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}07 -O ${OUTPUT}07 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}10 -O ${OUTPUT}10 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}11 -O ${OUTPUT}11 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}14 -O ${OUTPUT}14 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}17 -O ${OUTPUT}17 --clip 20 --skip 2 -a 2 --scale 256
$CMD -I ${VIDEOS}18 -O ${OUTPUT}18 --clip 20 --skip 2 -a 2 --scale 256

echo "Finished computing optical flow."