import os
import pandas as pd
import numpy as np
import argparse
import caffe
import lmdb
from utils import mkdir_p


def convert(lmdb_path, index_file, fill_length=None):
    
    # Load features from LMDB #
    ###########################

    print '[INFO] Load features...'

    with lmdb.open(lmdb_path) as lmdb_env:
        lmdb_txn = lmdb_env.begin()
        lmdb_cursor = lmdb_txn.cursor()
        datum = caffe.proto.caffe_pb2.Datum()

        data = []
        for i, (key, v) in enumerate(lmdb_cursor):
            datum.ParseFromString(v)
            data.append(np.asarray(datum.float_data))
            
            if i % 5000 == 0 and i > 0:
                print '[INFO] Read %d feature rows...' % i
    df = pd.DataFrame(data)
    
    # load index #
    ##############

    # peek inside to see if index has 2 or 3 columns:
    ncols = pd.read_csv(index_file, sep=' ', header=None, nrows=1).shape[1]

    if ncols == 2:
        # image list
        dfidx = pd.read_csv(index_file, sep=' ', header=None, names=['file'], usecols=[0])
        dfidx['video'] = dfidx['file'].apply(lambda p: os.path.basename(os.path.dirname(p)))
        dfidx['frame'] = dfidx['file'].apply(lambda p: int(os.path.splitext(os.path.basename(p))[0].split('_')[-1]))
        dfidx = dfidx.drop('file', axis=1)
    elif ncols == 3:
        # c3d image volume
        dfidx = pd.read_csv(index_file, sep=' ', header=None, names=['video', 'frame'], usecols=[0, 1])
    else:
        raise TypeError('Unknown index file format.')

    if df.shape[0] < dfidx.shape[0]:
        raise ValueError('Index is longer than data.')

    # merge index to data #
    #######################
    df = df.iloc[:dfidx.shape[0]].join(dfidx.loc[:, ['video', 'frame']]).set_index(['video', 'frame'])

    # fill missing index and interpolate #
    ######################################

    if fill_length is not None and fill_length > 1:
        videos = df.index.get_level_values('video').unique()

        dfinterp = {}
        for vid in videos:
            
            dftemp = df.loc[vid, :]
            dftemp = dftemp.reindex(range(dftemp.index.min(), 
                                          dftemp.index.max() + fill_length - 1)).interpolate()
            dftemp = dftemp.rolling(fill_length, center=True, min_periods=1).mean()  # smooth by window mean
            dfinterp[vid] = dftemp
          
        # merge back  
        df = pd.concat(dfinterp, names=['video', 'frame']).sortlevel(0)

    # make sure columns names are strings (for usage in ITT)
    df.columns = df.columns.astype(str)

    return df


def store(df, output_path):
    # output
    mkdir_p(output_path)
    videos = df.index.get_level_values('video').unique()
    for vid in videos:
        df.loc[vid, :].to_hdf(os.path.join(output_path, '{}.h5'.format(vid)), key='features')

    print '[INFO] Features stored in {}.'.format(output_path)


def convert_lmdb2h5(lmdb_path, index_file, output_path, fill_length=None):

    df = convert(lmdb_path, index_file, fill_length)
    store(df, output_path)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Loads (float) data from an LMDB, merges it with a specified index and stores everything in multiple HDFs (one file per first index level, e.g., video).')
    parser.add_argument('lmdb', type=str, help='Path to LMDB')
    parser.add_argument('index', type=str, help='Path to index file (e.g., train.txt as used in Caffe)')
    parser.add_argument('output', type=str, nargs='?', default='features_hdf', help='Output folder (default: <here>/features_hdf)')
    parser.add_argument('--fill', type=int, default=0, help='Complete index if index has gaps. Interpolate data in between. Value is number of frames over which to smooth interpolated values.')
    args = parser.parse_args()

    output_path = os.path.abspath(args.output)
    convert_lmdb2h5(args.lmdb, args.index, output_path, args.fill)

    print '[INFO] Done.'