#!/usr/bin/env sh
# This script converts the mnist data into lmdb/leveldb format,
# depending on the value assigned to $BACKEND.
set -e

# INPUT directory; converts every *.mp4 file in this folder
VIDEOS=~/datasets/RatSI/videos

# OUTPUT directory; a subfolder is created for every video file
VIDEO_FRAMES=~/datasets/RatSI/video_frames_576x576

# Set to other than 0 to export at different frame rate (e.g., set to 5 to export 5 frames per second)
FPS_OUT=0

# Set to true to export only first 30 seconds of each video file. Use for debug purposes.
TEST_OUT=false

# Verbosity level used for FFMPEG. 
FFMPEG_LOGLEVEL=20

# Set RESIZE=true to resize the images to 256x256 (or whatever you specify below). Leave as false if images have
# already been resized using another tool.
RESIZE=false

#######################################################################
if $RESIZE; then
	RESIZE_HEIGHT=128
	RESIZE_WIDTH=128
	RES_PARAM="-vf scale=${RESIZE_WIDTH}x${RESIZE_HEIGHT}"
else
	RESIZE_HEIGHT=0
	RESIZE_WIDTH=0
	RES_PARAM=""
fi

if $TEST_OUT; then
	T_PARAM="-t 30"
else
	T_PARAM=""
fi

if [ $FPS_OUT -gt 0 ]; then
	FPS_PARAM="-vf fps=${FPS_OUT}"
else
	FPS_PARAM=""
fi

echo "Converting ${VIDEOS} ..."
echo "Resizing images? ${RESIZE}"
if $RESIZE; then
	echo "Resize to ${RESIZE_WIDTH} x ${RESIZE_HEIGHT}"
fi

# loop over video files
for vidfile in ${VIDEOS}/*.mp4; do
	echo "  Converting ${vidfile} ..."
	OBSNAME=${vidfile##*/}
	OBSNAME=${OBSNAME%.*}
	
	mkdir -p ${VIDEO_FRAMES}/${OBSNAME}

	# FFMPEG frame extraction with resizing
	FFMPEG_CMD="ffmpeg -i ${VIDEOS}/${OBSNAME}.mp4 ${T_PARAM} ${FPS_PARAM} ${RES_PARAM} -qscale:v 5 -hide_banner -nostats -loglevel ${FFMPEG_LOGLEVEL} ${VIDEO_FRAMES}/${OBSNAME}/frame_%08d.jpg"
	
	# run ffmpeg to export video frames
	${FFMPEG_CMD}
	echo "  Done."
done

echo "Finished converting videos to images."
