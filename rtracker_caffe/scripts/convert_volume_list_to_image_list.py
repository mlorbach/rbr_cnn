import argparse
import pandas as pd


def convert(file_in, file_out, segment_length):

    dfin = pd.read_csv(file_in, sep=' ', index_col=None,
                       names=['video', 'start_frame', 'label'])

    # pick frame at center of segment if wanted
    if segment_length > 1:
        dfin['start_frame'] += segment_length / 2

    dfin['start_frame'] = dfin['start_frame'].apply(lambda i: 'frame_{:08d}.jpg'.format(i))
    dfin['img'] = dfin['video'] + '/' + dfin['start_frame']
    dfin = dfin.loc[:, ['img', 'label']].set_index('img')

    dfin.to_csv(file_out, sep=' ', header=None)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('volume_list', type=str,
                        help='Path to volume list file (C3D format)')
    parser.add_argument('output_list', type=str,
                        help='Output file')
    parser.add_argument('--seg_length', '-l', type=int, default=0,
                        help='If > 0, output image offset by length/2 (center frame of segment).')
    args = parser.parse_args()

    convert(args.volume_list, args.output_list, args.seg_length)

    print 'Done. Written file list to: "{}"'.format(args.output_list)
