#!/usr/bin/env sh
# This script uses tracking information to create image patches from the bounding box around the tracked points.
# All videos (video_frames) in the input directory are processed in one batch.
set -e

# INPUT directory
VIDEOS=~/datasets/RatSI/videos

TRACKING=~/datasets/RatSI/tracking

# OUTPUT directory; a subfolder is created for every video file and subject
OUTPUT=~/datasets/RatSI/box_frames_112

CMD=~/rbr_cnn/volume_extractor/build/VolumeExtractorVid


#######################################################################

echo "Converting everything in ${VIDEOS} ..."


for video in `find ${VIDEOS}  -maxdepth 1 -name "*.mp4" -type f -exec basename {} \;`; do
	echo "  Create for ${video}"
	OBSNAME=${video%.*}
	${CMD} -I ${VIDEOS}/${video} -t ${TRACKING}/${OBSNAME}.csv -O ${OUTPUT} -s 112
done

echo "Finished creating image patches."