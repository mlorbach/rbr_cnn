import caffe
import lmdb
import argparse

#test_set_lmdb_path = '/home/malte/rbr_cnn/rtracker_caffe/data/ratsi_test_lmdb'


def get_lmdb_entries(lmdb_path):
    with lmdb.open(lmdb_path) as lmdb_env:
        lmdb_txn = lmdb_env.begin()
        lmdb_cursor = lmdb_txn.cursor()
        datum = caffe.proto.caffe_pb2.Datum()

        data = []
        for key, v in lmdb_cursor:
            datum.ParseFromString(v)
            data.append((key.split('_', 1)[-1], datum.label))
    return data


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('lmdb', type=str, help='Path to LMDB')
    parser.add_argument('output', type=str, nargs='?', default='lmdb_index.txt', help='Output file')
    args = parser.parse_args()

    data = get_lmdb_entries(args.lmdb)

    with open(args.output, 'wb') as fh:
        for k, v in data:
            fh.write('%s %d\n' % (k, v))

    print 'Done. Written index to: "{}"'.format(args.output)