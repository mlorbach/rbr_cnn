import os
import sys
from glob import glob
import numpy as np
import pandas as pd
import json
import scipy.stats
import argparse
from utils import mkdir_p, label_merge_transform
from compute_class_weights import compute_class_weights
from sklearn.preprocessing import LabelEncoder


def make_aux(dataset_path, output_path, merge_meta=False, drop_na=False, 
             drop_class=None, segment_iter=5, segment_length=10):

    dataset_path = os.path.normpath(os.path.expanduser(dataset_path))
    labels_path = os.path.join(dataset_path, 'annotations')
    images_path = os.path.join(dataset_path, 'video_frames')

    if not os.path.isdir(images_path):
        raise "Could not find images in {}.".format(images_path)
    if not os.path.isdir(labels_path):
        raise "Could not find label data in {}.".format(labels_path)

    all_videos = []
    videos = {}

    for split in ['train', 'val', 'test']:
        print 'Reading {} video list...'.format(split)
        with open(os.path.join(dataset_path, 'dinfo_{}.json'.format(split)), 'r') as f:
            dinfo = json.load(f)
        all_videos += dinfo['video_names']
        videos[split] = dinfo['video_names']
        print 'Found {} {} videos.'.format(len(videos[split]), split)

    print 'Total videos: {}'.format(len(all_videos))

    # Create output path if non-existent
    output_path = os.path.normpath(os.path.expanduser(output_path))
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    ###############
    ## LOAD DATA ##
    ###############

    Y = {}

    # load labels

    # annotations
    for vid in all_videos:
        Y[vid] = pd.read_csv(os.path.join(labels_path, vid + '.csv'), sep=';', index_col=0)
    Y = pd.concat(Y, names=['video', 'frame'])

    # merge contact and solitary classes if required
    if merge_meta:

        '''
        approach
        attack
        copulation
        chase
        circle
        drink
        eat
        clean
        human
        sniff
        up
        walk_away
        other
        '''

        # print 'Merging contact classes ...'
        # Y['action'] = label_merge_transform(Y['action'],
        #                                     ['attack',
        #                                     'copulation'],
        #                                     'contact')
        print 'Merging solitary classes ...'
        Y['action'] = label_merge_transform(Y['action'],
                                            ['drink',
                                            'eat',
                                            'clean',
                                            'up'],
                                            'solitary')

    # convert string labels to numeric labels
    le = LabelEncoder()
    Y['label'] = le.fit_transform(Y['action'])
    Y = Y.drop('action', axis=1)

    n_classes = len(le.classes_)
    print 'Labels:', le.classes_.tolist()

    if 'human' in le.classes_:
        ignore_label = le.transform(['human']).tolist()
    else:
        ignore_label = []

    if 'other' in le.classes_:
        neutral_label = le.transform(['other']).tolist()
    else:
        neutral_label = []

    # output label list
    np.savetxt(os.path.join(output_path, 'labels.txt'), le.classes_, fmt='%s')

    # compute majority vote over sliding windows
    Y['label'] = Y['label'].rolling(segment_length,
                                    center=True,
                                    min_periods=1).apply(
                                        lambda m: scipy.stats.mode(m)[0][0]
                                    ).astype(int)

    #########################
    ## PROCESS IMAGE FILES ##
    #########################

    # create list of image files
    filelist = {}

    for vid in all_videos:
        data = sorted(glob(os.path.join(images_path, vid, '*.jpg')))

        N = len(data)
        if N == 0:
            print 'Could not find any images in {}'.format(os.path.join(images_path, vid, '*.jpg'))
            sys.exit(-1)

        first_frame = int(os.path.splitext(os.path.basename(data[0]))[0].split('_')[-1])
        last_frame = int(os.path.splitext(os.path.basename(data[-1]))[0].split('_')[-1])

        if N != (last_frame - first_frame + 1):
            raise('Probably missing images in {}'.format(vid))

        # specify only the start frame of each segment
        #  making sure that the last segment actually is segment_length long
        #  (could be that we miss the last few frames, if the last segment would be too short) 
        frame_range = np.arange(first_frame, last_frame + 1 - (segment_length - 1), segment_iter)

        # we set the index to the start_frame plus half the segment length (that is the center of the segment)
        # we'll later join the label at the index frame, so that we store the label of the center of the segment
        filelist[vid] = pd.DataFrame(data=frame_range, index=(frame_range + segment_length / 2).astype(int), columns=['start_frame'])
        filelist[vid]['path'] = vid #os.path.join(images_path, vid)

    # all files into one dataframe
    filelist = pd.concat(filelist, names=['video', 'frame'])
    #filelist['path'] = filelist['path'].apply(lambda p: os.path.normpath(p.split(dataset_path + '/', 1)[1]))


    #############################
    # join labels and image files
    #############################

    data = filelist.join(Y, how='left')


    #############################
    ## Data consistency checks ##
    #############################

    if drop_na:
        print 'Dropping {} rows.'.format(data.isnull().any(axis=1).sum())
        data = data.dropna()  # drop images without labels/labels without images

    # check if data is complete
    assert data.notnull().all().all(), 'Shape of labels and images do not match. Missing either labels or images!'

    ############################
    ## BALANCING              ##
    ############################

    if drop_class is not None and len(drop_class) > 0:

        for split, dropdict in drop_class.viewitems():

            if dropdict == 'min':

                # balance by dropping

                ycount = np.bincount(data.loc[videos[split], 'label'].values, minlength=n_classes)

                mask = ycount > 0  # must have at least one sample
                if len(ignore_label) > 0:
                    mask[ignore_label] = False  # ignore

                min_y = np.min(ycount[mask])

                drop_per_y = ycount - min_y
                drop_per_y[drop_per_y < 0] = 0

                print 'Counts: {}'.format(ycount)
                print 'Dropping Ns: {}'.format(drop_per_y)

                for y, n in enumerate(drop_per_y):

                    if n == 0:
                        continue

                    idx = data.loc[videos[split], :].loc[data.loc[videos[split], 'label'] == y].index.values
                    np.random.shuffle(idx)
                    idx = idx[:n]

                    print 'Dropping {} rows of class "{}" in {} set...'.format(len(idx), le.inverse_transform([y])[0], split)
                    data = data.drop(idx)

            elif dropdict == 'max':

                # balance by replication
                ycount = np.bincount(data.loc[videos[split], 'label'].values, minlength=n_classes)

                mask = ycount > 0  # must have at least one sample
                if len(ignore_label) > 0:
                    mask[ignore_label] = False  # ignore

                max_y = np.max(ycount[mask])

                rep_per_y = max_y - ycount
                rep_per_y[rep_per_y < 0] = 0
                rep_per_y[~mask] = 0  # don't replicate ignore_label

                print 'Counts: {}'.format(ycount)
                print 'Replicating Ns: {}'.format(rep_per_y)

                for y, n in enumerate(rep_per_y):

                    if n == 0:
                        continue

                    idx = data.loc[videos[split], :].loc[data.loc[videos[split], 'label'] == y].index.values

                    K = idx.shape[0]
                    N_tiles = int(max_y / K) - 1
                    N_rem = max_y % K

                    idx2 = np.concatenate((np.tile(idx, N_tiles), np.random.choice(idx, N_rem)))

                    dfrep = data.loc[idx2]
                    maxf = data.loc[videos[split]].index.get_level_values('frame').max() + 1
                    dfrep = dfrep.reset_index('frame')
                    dfrep['frame'] = np.arange(dfrep.shape[0]) + maxf
                    dfrep = dfrep.set_index('frame', append=True)

                    print 'Replicating {} rows of class "{}" in {} set...'.format(len(idx2), le.inverse_transform([y])[0], split)
                    data = data.append(dfrep).sortlevel(0)

            else:
                for c, ratio in dropdict.viewitems():

                    cnum = le.transform([c])
                    idx = data.loc[videos[split], :].loc[data.loc[videos[split], 'label'] == cnum[0]].index.values
                    np.random.shuffle(idx)
                    idx = idx[:int(ratio * len(idx))]

                    print 'Dropping {} rows of class "{}" in {} set...'.format(len(idx), c, split)
                    n_before = data.shape[0]
                    data = data.drop(idx)  #
                    assert n_before - data.shape[0] == len(idx), 'Inconsistent data shape after dropping.'


    ############################
    ## OUTPUT IMG/LABEL FILES ##
    ############################

    # split into train, test, val and export to .txt files

    export_cols = ['path', 'start_frame', 'label']

    for split, vidnames in videos.viewitems():
        print 'Export {}...'.format(split)
        data.loc[vidnames, :].to_csv(os.path.join(output_path, '{}.txt'.format(split)),
                                     sep=' ', header=None, float_format='%.12g',
                                     index=False, columns=export_cols)

        ycount = np.bincount(data.loc[vidnames, 'label'].values, minlength=n_classes)
        np.savetxt(os.path.join(output_path, 'label_distribution_{}.txt'.format(split)), ycount, fmt='%d')


    # for training set, compute infogain matrix
    print 'Compute infogain matrix for training set...'
    compute_class_weights(os.path.join(output_path, 'label_distribution_train.txt'),
                          os.path.join(output_path, 'infogain.binaryproto'),
                          ignore_label=ignore_label,
                          neutral_label=neutral_label)

    return


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', type=str, help='Root path to RatSI dataset')
    parser.add_argument('output', type=str, help='Output folder for generated auxiliary data. Will be created if it does not exist.')
    parser.add_argument('-m', '--merge_meta', action='store_true', help='Merge contact and solitary action classes into general "contact"/"solitary" classes.')
    parser.add_argument('-d', '--drop', action='store_true', help='Drop samples with missing images/labels/tracking.')
    parser.add_argument('-b', '--balance', action='count', help='Balance training set by dropping majority class samples.')
    args = parser.parse_args()

    data_path = args.dataset
    output_path = args.output
    drop = args.drop
    merge = args.merge_meta

    if args.balance is not None:
        if args.balance == 1:
            drop_class = {'train': {'solitary': 0.7}, 'val': {'solitary': 0.5}}
        elif args.balance == 2:
            # balance entirely
            drop_class = {'train': 'min', 'val': {'other': 0.5}}
        elif args.balance > 2:
            # replicate
            drop_class = {'train': 'max', 'val': 'max'}
    else:
        drop_class = None
    
    print 'Create auxiliary data for RatSI dataset in {}'.format(output_path)

    make_aux(data_path, output_path, merge_meta=merge, drop_na=drop, drop_class=drop_class, 
             segment_iter=5, segment_length=10)
    print 'Done.'
