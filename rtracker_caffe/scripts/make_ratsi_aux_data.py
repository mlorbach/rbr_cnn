import os
import sys
from glob import glob
import numpy as np
import pandas as pd
import argparse
from utils import mkdir_p
from compute_class_weights import compute_class_weights
from sklearn.preprocessing import LabelEncoder


def label_merge_transform(Y, labels_to_merge, group_label=None):

    n_labels = len(labels_to_merge)

    if n_labels == 0:
        return Y

    if group_label is None:
        if n_labels > 0:
            group_label = labels_to_merge[0]
        else:
            group_label = 'merged'

    if n_labels == 2 and group_label == labels_to_merge[0]:
        Y[ Y==labels_to_merge[1] ] = group_label
    else:
        mask = np.in1d( Y, labels_to_merge )
        Y[ mask ] = group_label

    return Y


def make_aux(dataset_path, output_path, output=['label'],
             merge_contact=False, tracking_labels=False,
             drop_na=False, drop_class=None, tracking_scale=1.):

    dataset_path = os.path.normpath(os.path.expanduser(dataset_path))

    tracking_path = os.path.join(dataset_path, 'tracking')
    labels_path = os.path.join(dataset_path, 'annotations')
    images_path = os.path.join(dataset_path, 'video_frames')
    flow_path = os.path.join(dataset_path, 'flow_frames')

    if not os.path.isdir(images_path):
        raise "Could not find images in {}.".format(images_path)
    if 'tracks' in output and not os.path.isdir(tracking_path):
        raise "Could not find tracking data in {}.".format(tracking_path)
    if 'label' in output and not os.path.isdir(labels_path):
        raise "Could not find label data in {}.".format(labels_path)
    if 'flow' in output and not os.path.isdir(flow_path):
        raise "Could not find flow images in {}.".format(flow_path)

    all_videos = ['Observation01',
                  'Observation02',
                  'Observation04',
                  'Observation07',
                  'Observation10',
                  'Observation11',
                  'Observation14',
                  'Observation17',
                  'Observation18']
    # obs 1-8 == SCA, obs 9-18 == WT

    videos = {
              'train': 
                ['Observation01','Observation02', 'Observation10','Observation11','Observation14'],
              'test':
                ['Observation07', 'Observation18'],
              'val':
                ['Observation04', 'Observation17']
    }

    # Create output path if non-existent
    output_path = os.path.normpath(os.path.expanduser(output_path))
    if not os.path.exists(output_path):
        os.mkdir(output_path)


    ###############
    ## LOAD DATA ##
    ###############

    Y = {}
    T = {}

    # load labels
    if 'label' in output:
        # annotations
        for vid in all_videos:
            Y[vid] = pd.read_csv(os.path.join(labels_path, vid + '.csv'), sep=';', index_col=0)
        Y = pd.concat(Y, names=['video', 'frame'])

        # merge contact classes if required
        if merge_contact:
            print 'Merging contact classes ...'
            Y['action'] = label_merge_transform(Y['action'],
                                                ['Allogrooming',
                                                'Nape attacking',
                                                'Pinning',
                                                'Social Nose Contact',
                                                'Other'],
                                                'Contact')
        # convert string labels to numeric labels
        le = LabelEncoder()
        Y['label'] = le.fit_transform(Y['action'])
        Y = Y.drop('action', axis=1)

        n_classes = len(le.classes_)
        print 'Labels:', le.classes_.tolist()

        # output label list
        np.savetxt(os.path.join(output_path, 'labels.txt'), le.classes_, fmt='%s')

    if 'tracks' in output:
        # tracking
        for vid in all_videos:
            T[vid] = pd.read_csv(os.path.join(tracking_path, vid + '.csv'), sep=';', index_col=0, tupleize_cols=True, header=[0,1], dtype='float32')
            #T[vid].columns = pd.MultiIndex.from_tuples(T[vid].columns)
        T = pd.concat(T, names=['video', 'frame'])
        track_cols = ['center_m_x', 'center_m_y', 'nose_m_x', 'nose_m_y', 'tail_m_x', 'tail_m_y',
        'center_w_x', 'center_w_y', 'nose_w_x', 'nose_w_y', 'tail_w_x', 'tail_w_y']
        T.columns = track_cols

        # drop tracking point that we do not want:
        track_cols = [c for c in track_cols if c[0] in tracking_labels] # first letter of column name must be either 'c', 'n', or 't'
        T = T.loc[:, track_cols]

        T /= tracking_scale


    #########################
    ## PROCESS IMAGE FILES ##
    #########################

    # create list of image files
    filelist = {}
    flowlist = {}

    for vid in all_videos:
        data = sorted(glob(os.path.join(images_path, vid, '*.jpg')))
        framenr = map(lambda p: int(os.path.splitext(os.path.basename(p))[0].split('_')[-1]) - 1, data)
        filelist[vid] = pd.Series(data=data, index=framenr, name='image')

        if 'flow' in output:
            data = sorted(glob(os.path.join(flow_path, vid, '*.flo')))
            framenr = map(lambda p: int(os.path.splitext(os.path.basename(p))[0].split('_')[-1]) - 1, data)
            flowlist[vid] = pd.Series(data=data, index=framenr, name='flow')

    # all files into one dataframe
    filelist = pd.concat(filelist, names=['video', 'frame'])
    filelist = filelist.apply(lambda p: os.path.normpath(p.split(dataset_path + '/', 1)[1]))

    # merge with flow files if available
    if 'flow' in output:
        flowlist = pd.concat(flowlist, names=['video', 'frame'])
        flowlist = flowlist.apply(lambda p: os.path.normpath(p.split(dataset_path + '/', 1)[1]))
        filelist = pd.concat([filelist, flowlist], axis=1)

    if not isinstance(filelist, pd.DataFrame):
        filelist = pd.DataFrame(filelist)

    #############################
    # join labels and image files
    #############################

    data = None
    if len(Y) > 0:
        data = filelist.join(Y)

        if len(T) > 0:
            if data is None:
                data = filelist.join(T)
            else:
                data = data.join(T)

    #############################
    ## Data consistency checks ##
    #############################

    if drop_na:
        print 'Dropping {} rows.'.format(data.isnull().any(axis=1).sum())
        data = data.dropna()  # drop images without labels/labels without images

    # check if data is complete
    assert data.notnull().all().all(), 'Shape of labels and images do not match. Missing either labels or images!'


    ############################
    ## BALANCING              ##
    ############################


    ############################
    ## BALANCING              ##
    ############################

    if drop_class is not None and len(drop_class) > 0:

        for split, dropdict in drop_class.viewitems():

            if dropdict == 'min':

                ycount = np.bincount(data.loc[videos[split], 'label'].values, minlength=n_classes)

                ignore_label = le.transform(['Uncertain']).tolist()
                mask = ycount > 0  # must have at least one sample
                if len(ignore_label) > 0:
                    mask[ignore_label] = False  # ignore

                min_y = np.min(ycount[mask])

                drop_per_y = ycount - min_y
                drop_per_y[drop_per_y < 0] = 0

                print 'Counts: {}'.format(ycount)
                print 'Dropping Ns: {}'.format(drop_per_y)

                for y, n in enumerate(drop_per_y):

                    idx = data.loc[videos[split], :].loc[data.loc[videos[split], 'label'] == y].index.values
                    np.random.shuffle(idx)
                    idx = idx[:n]

                    print 'Dropping {} rows of class "{}" in {} set...'.format(len(idx), le.inverse_transform([y])[0], split)
                    data = data.drop(idx)

            else:
                for c, ratio in dropdict.viewitems():

                    cnum = le.transform([c])
                    idx = data.loc[videos[split], :].loc[data.loc[videos[split], 'label'] == cnum[0]].index.values
                    np.random.shuffle(idx)
                    idx = idx[:int(ratio * len(idx))]

                    print 'Dropping {} rows of {} class in {} set...'.format(len(idx), c, split)
                    n_before = data.shape[0]
                    data = data.drop(idx)  #
                    assert n_before - data.shape[0] == len(idx), 'Inconsistent data shape after dropping.'


    ############################
    ## OUTPUT IMG/LABEL FILES ##
    ############################

    # split into train, test, val and export to .txt files

    export_cols = ['image']
    if 'flow' in output:
        export_cols.append('flow')

    if 'label' in output:
        export_cols.append('label')

    if 'tracks' in output and tracking_labels:
        export_cols += track_cols

    for split, vidnames in videos.viewitems():
        print 'Export {}...'.format(split)
        data.loc[vidnames, :].to_csv(os.path.join(output_path, '{}.txt'.format(split)),
                                     sep=' ', header=None, float_format='%.12g',
                                     index=False, columns=export_cols)

        if 'label' in output:
            ycount = np.bincount(data.loc[vidnames, 'label'].values, minlength=n_classes)
            np.savetxt(os.path.join(output_path, 'label_distribution_{}.txt'.format(split)), ycount, fmt='%d')

    if 'label' in output:
        # for training set, compute infogain matrix
        print 'Compute infogain matrix for training set...'
        compute_class_weights(os.path.join(output_path, 'label_distribution_train.txt'),
                              os.path.join(output_path, 'ratsi_infogain.binaryproto'),
                              ignore_label=le.transform(['Uncertain']).tolist())#,
                              # neutral_label=le.transform(['Other']).tolist())

    return


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', type=str, help='Root path to RatSI dataset')
    parser.add_argument('output', type=str, help='Output folder for generated auxiliary data. Will be created if it does not exist.')
    parser.add_argument('--actions', action='store_true', help="Include action class labels in file list")
    parser.add_argument('--flow', action='store_true', help="Include optical flow images in file list")
    parser.add_argument('-t', '--tracking', nargs='*', choices=['c','n','t'], help='Include tracking positions in file list (for regression).')
    parser.add_argument('--scale', type=float, default=1., help='Scale of tracking data. Tracking will be devided by `scale` before export.')
    parser.add_argument('-m', '--merge_contact', action='store_true', help='Merge contact action classes into general "contact" class.')
    parser.add_argument('-d', '--drop', action='store_true', help='Drop samples with missing images/labels/tracking.')
    parser.add_argument('-b', '--balance', action='count', help='Balance training set by dropping majority class samples.')
    args = parser.parse_args()

    data_path = args.dataset
    output_path = args.output
    drop = args.drop
    merge = args.merge_contact
    tracking_scale = args.scale
    if args.tracking is None:
        tracks = False
    elif len(args.tracking) == 0:
        tracks = ['c', 'n', 't']
    else:
        tracks = list(set(args.tracking))

    print 'Create auxiliary data for RatSI dataset in {}'.format(output_path)

    output = []
    if args.actions:
        output.append('label')
    if args.flow:
        output.append('flow')
    if tracks is not False and len(tracks) > 0:
        output.append('tracks')

    if args.balance is not None:
        if args.balance == 1:
            drop_class = {'train': {'Solitary': 0.75}, 'val': {'Solitary': 0.5}}
        elif args.balance > 1:
            # balance entirely
            drop_class = {'train': 'min', 'val': {'Solitary': 0.5}}
    else:
        drop_class = None

    make_aux(data_path, output_path, output=output, merge_contact=merge, tracking_labels=tracks, drop_na=drop, drop_class=drop_class, tracking_scale=tracking_scale)
    print 'Done.'
