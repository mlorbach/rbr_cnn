import os
import sys
from glob import glob
import numpy as np
import pandas as pd
import scipy.stats
import h5py
import argparse
from utils import mkdir_p
from compute_class_weights import compute_class_weights
from sklearn.preprocessing import LabelEncoder


def label_merge_transform(Y, labels_to_merge, group_label=None):

    n_labels = len(labels_to_merge)

    if n_labels == 0:
        return Y

    if group_label is None:
        if n_labels > 0:
            group_label = labels_to_merge[0]
        else:
            group_label = 'merged'

    if n_labels == 2 and group_label == labels_to_merge[0]:
        Y[ Y==labels_to_merge[1] ] = group_label
    else:
        mask = np.in1d( Y, labels_to_merge )
        Y[ mask ] = group_label

    return Y


def make_aux(dataset_path, output_path, merge_contact=False, drop_na=False, drop_class=None, segment_iter=5, segment_length=10, shuffle=['train', 'val']):

    dataset_path = os.path.normpath(os.path.expanduser(dataset_path))
    labels_path = os.path.join(dataset_path, 'annotations')
    images_path = os.path.join(dataset_path, 'box_frames_112')

    if not os.path.isdir(images_path):
        raise "Could not find images in {}.".format(images_path)
    if not os.path.isdir(labels_path):
        raise "Could not find label data in {}.".format(labels_path)

    all_videos = ['Observation01',
                  'Observation02',
                  'Observation04',
                  'Observation07',
                  'Observation10',
                  'Observation11',
                  'Observation14',
                  'Observation17',
                  'Observation18']
    # obs 1-8 == SCA, obs 9-18 == WT

    videos = {
              'train': 
                ['Observation01', 'Observation02', 'Observation10', 'Observation11', 'Observation14'],
              'test':
                ['Observation07', 'Observation18'],
              'val':
                ['Observation04', 'Observation17']
    }

    # Create output path if non-existent
    output_path = os.path.normpath(os.path.expanduser(output_path))
    if not os.path.exists(output_path):
        os.mkdir(output_path)


    ###############
    ## LOAD DATA ##
    ###############

    # ----------------------
    # load labels
    # ----------------------

    Y = {}

    # annotations
    for vid in all_videos:
        Y[vid] = pd.read_csv(os.path.join(labels_path, vid + '.csv'), sep=';', index_col=0)
    Y = pd.concat(Y, names=['video', 'frame'])

    # merge contact classes if required
    if merge_contact:
        print 'Merging contact classes ...'
        Y['action'] = label_merge_transform(Y['action'],
                                            ['Allogrooming',
                                            'Nape attacking',
                                            'Pinning',
                                            'Social Nose Contact',
                                            'Other'],
                                            'Contact')
    # convert string labels to numeric labels
    le = LabelEncoder()
    Y['label'] = le.fit_transform(Y['action'])
    Y = Y.drop('action', axis=1)

    n_classes = len(le.classes_)
    print 'Labels:', le.classes_.tolist()

    # output label list
    np.savetxt(os.path.join(output_path, 'labels.txt'), le.classes_, fmt='%s')

    # compute majority vote over sliding windows
    Y['label'] = Y['label'].rolling(segment_length,
                                    center=True,
                                    min_periods=1).apply(
                                        lambda m: scipy.stats.mode(m)[0][0]
                                    ).astype(int)


    # ----------------------
    # read tracking features
    # ----------------------

    T = {}

    for vid in all_videos:
        T[vid] = pd.read_csv(os.path.join(images_path, '{}.csv'.format(vid)), sep=';', index_col=0, names=['dist', 'rel_orientation', 'v0', 'v1'])
        T[vid] = T[vid].drop('rel_orientation', axis=1) # not using orientation as it is the orientation to the image-axis

        # smooth velocity to the average of the segment length
        T[vid].loc[:, ['v0', 'v1']] = T[vid].loc[:, ['v0', 'v1']].rolling(segment_length, min_periods=1, center=True).mean()

        # compute derivatives
        Tdt = T[vid].diff().fillna(0)
        Tdt.columns = map(lambda c: c + '_dt', Tdt.columns)
        T[vid] = T[vid].join(Tdt)

        # smooth derivatives to the average of the segment length
        T[vid].loc[:, ['v0_dt', 'v1_dt']] = T[vid].loc[:, ['v0_dt', 'v1_dt']].rolling(segment_length, min_periods=1, center=True).mean()

        # make index start with 1
        T[vid].index += 1

    T = pd.concat(T, names=['video', 'frame']).astype('f4')  # make float32 to save space


    #########################
    ## PROCESS IMAGE FILES ##
    #########################

    # create list of image files
    filelist = {}

    for vid in all_videos:

        data = sorted(glob(os.path.join(images_path, 'subject_0', vid, '*.jpg')))

        N = len(data)
        if N == 0:
            print 'Could not find any images in {}'.format(os.path.join(images_path, vid, 'subject_0', '*.jpg'))
            sys.exit(-1)

        first_frame = int(os.path.splitext(os.path.basename(data[0]))[0].split('_')[-1]) + 1  # box_frames start with 0
        last_frame = int(os.path.splitext(os.path.basename(data[-1]))[0].split('_')[-1]) + 1

        if N != (last_frame - first_frame + 1):
            raise('Probably missing images in {}'.format(vid))

        # specify only the start frame of each segment
        #  making sure that the last segment actually is segment_length long
        #  (could be that we miss the last few frames, if the last segment would be too short) 
        frame_range = np.arange(first_frame, last_frame + 1 - (segment_length - 1), segment_iter)

        # we set the index to the start_frame plus half the segment length (that is the center of the segment)
        # we'll later join the label at the index frame, so that we store the label of the center of the segment
        filelist[vid] = pd.DataFrame(data=frame_range, index=(frame_range + segment_length / 2).astype(int), columns=['start_frame'])
        filelist[vid]['path'] = vid

    # all files into one dataframe
    filelist = pd.concat(filelist, names=['video', 'frame'])


    #############################
    # join labels and image files
    #############################

    data = filelist.join(Y, how='left')



    #############################
    ## Data consistency checks ##
    #############################

    if drop_na:
        print 'Dropping {} rows.'.format(data.isnull().any(axis=1).sum())
        data = data.dropna()  # drop images without labels/labels without images

    # check if data is complete
    assert data.notnull().all().all(), 'Shape of labels and images do not match. Missing either labels or images!'

    common_index = T.index.intersection(data.index)

    # this effectively leaves us with one value every segment_iter frames
    #  because of our smoothing earlier, T now contains the average value over the segment of length: segment_length
    T = T.reindex(common_index)
    data = data.reindex(common_index)


    ############################
    ## BALANCING              ##
    ############################

    if drop_class is not None and len(drop_class) > 0:

        for split, dropdict in drop_class.viewitems():
            for c, ratio in dropdict.viewitems():

                cnum = le.transform([c])
                idx = data.loc[videos[split], :].loc[data.loc[videos[split], 'label'] == cnum[0]].index.values
                np.random.shuffle(idx)
                idx = idx[:int(ratio * len(idx))]

                print 'Dropping {} rows of {} class in {} set...'.format(len(idx), c, split)
                n_before = data.shape[0]
                data = data.drop(idx)  #
                assert n_before - data.shape[0] == len(idx), 'Inconsistent data shape after dropping.'

                n_before = T.shape[0]
                T = T.drop(idx)  #
                assert n_before - T.shape[0] == len(idx), 'Inconsistent T shape after dropping.'


    ############################
    # Scaling                 ##
    ############################

    # scaling tracking features to 0-mean, 1-variance using only training videos
    T_mean = T.loc[videos['train'], :].mean(axis=0)
    T_var  = T.loc[videos['train'], :].var(axis=0)
    T = (T - T_mean) / np.sqrt(T_var)  # z-scale

    ############################
    ## OUTPUT IMG/LABEL FILES ##
    ############################

    # split into train, test, val and export to .txt files

    export_cols = ['path', 'start_frame', 'label']

    for split, vidnames in videos.viewitems():
        print 'Export {}...'.format(split)

        idx = data.loc[vidnames, :].index.values.copy()
        if split in shuffle:
            print '  Shuffle.'            
            np.random.shuffle(idx)

        data.loc[idx].to_csv(os.path.join(output_path, '{}.txt'.format(split)),
                                     sep=' ', header=None, float_format='%.12g',
                                     index=False, columns=export_cols)

        ycount = np.bincount(data.loc[vidnames, 'label'].values, minlength=n_classes)
        np.savetxt(os.path.join(output_path, 'label_distribution_{}.txt'.format(split)), ycount, fmt='%d')

        # -------------------------------
        # make HDF with tracking features
        # -------------------------------
        with h5py.File(os.path.join(output_path, '{}_tracking_features.h5'.format(split)), 'w') as fh5:
            # write tracking features
            # note that we use the possibly shuffled index, so that the data is in the same order as the images
            fh5.create_dataset('feats_tracking', 
                               data=T.loc[idx].values, 
                               dtype='f4')

            # also put the labels
            fh5.create_dataset('label', 
                               data=data.loc[idx, 'label'].values.astype(int), 
                               dtype='i')

        # list of the h5 files, we will load in caffe (it's only one here)
        with open(os.path.join(output_path, '{}_tracking_features.lst'.format(split)), 'w') as fh:
            fh.write(os.path.abspath(os.path.join(output_path, '{}_tracking_features.h5'.format(split))))


    # for training set, compute infogain matrix
    print 'Compute infogain matrix for training set...'
    compute_class_weights(os.path.join(output_path, 'label_distribution_train.txt'),
                          os.path.join(output_path, 'ratsi_infogain.binaryproto'),
                          ignore_label=le.transform(['Uncertain']).tolist())#,
                          # neutral_label=le.transform(['Other']).tolist())

    return


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', type=str, help='Root path to RatSI dataset')
    parser.add_argument('output', type=str, help='Output folder for generated auxiliary data. Will be created if it does not exist.')
    parser.add_argument('-m', '--merge_contact', action='store_true', help='Merge contact action classes into general "contact" class.')
    parser.add_argument('-d', '--drop', action='store_true', help='Drop samples with missing images/labels/tracking.')
    parser.add_argument('-b', '--balance', action='store_true', help='Balance training set by dropping majority class samples.')
    args = parser.parse_args()

    data_path = args.dataset
    output_path = args.output
    drop = args.drop
    merge = args.merge_contact

    if args.balance:
        drop_class = {'train': {'Solitary': 0.75}, 'val': {'Solitary': 0.5}}
    else:
        drop_class = None
    
    print 'Create auxiliary data for RatSI dataset in {}'.format(output_path)

    make_aux(data_path, output_path, merge_contact=merge, drop_na=drop, drop_class=drop_class, segment_iter=3, segment_length=12)
    print 'Done.'
