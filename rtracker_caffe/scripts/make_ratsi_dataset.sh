#!/usr/bin/env sh
# Create the imagenet lmdb inputs
# N.B. set the path to the imagenet train + val data dirs
set -e


DATA_ROOT=~/datasets/RatSI
OUTPUT=~/rbr_cnn/rtracker_caffe/data_balanced
TOOLS=build/tools

python ~/rbr_cnn/rtracker_caffe/scripts/make_ratsi_aux_data.py ${DATA_ROOT} ${OUTPUT} -m -d -bb --actions


echo "Creating train lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
	--gray \
    --shuffle \
    --encoded \
    $DATA_ROOT/ \
    $OUTPUT/train.txt \
    $OUTPUT/ratsi_train_lmdb
    
echo "Creating test lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
	--gray \
    --shuffle \
    --encoded \
    $DATA_ROOT/ \
    $OUTPUT/test.txt \
    $OUTPUT/ratsi_test_lmdb

echo "Creating val lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset \
	--gray \
    --shuffle \
    --encoded \
    $DATA_ROOT/ \
    $OUTPUT/val.txt \
    $OUTPUT/ratsi_val_lmdb

echo "Done."
