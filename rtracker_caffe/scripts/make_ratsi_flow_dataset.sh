#!/usr/bin/env sh
# Create the imagenet lmdb inputs
# N.B. set the path to the imagenet train + val data dirs
set -e

DATA_ROOT=~/datasets/RatSI
OUTPUT=~/rbr_cnn/rtracker_caffe/data_flow
TOOLS=build/tools
FLOW_CLIP_VALUE=32
RESIZE_FLOW=64

PARALLEL=true
if [ $# -gt 0 ] && [ $1="noparallel" ]; then
    echo "Switching off parallel processing."
    PARALLEL=false
fi

python ~/rbr_cnn/rtracker_caffe/scripts/make_ratsi_aux_data.py ${DATA_ROOT} ${OUTPUT} --actions --flow -t c -m -d --scale 576

echo "Removing previous databases..."
rm -rf $OUTPUT/ratsi_train_lmdb
rm -rf $OUTPUT/ratsi_train_lmdb_flow
rm -rf $OUTPUT/ratsi_train_lmdb_tracks
rm -rf $OUTPUT/ratsi_test_lmdb
rm -rf $OUTPUT/ratsi_test_lmdb_flow
rm -rf $OUTPUT/ratsi_test_lmdb_tracks
rm -rf $OUTPUT/ratsi_val_lmdb
rm -rf $OUTPUT/ratsi_val_lmdb_flow
rm -rf $OUTPUT/ratsi_val_lmdb_tracks

if [ "$PARALLEL" = true ]; then
    echo "Create databases in parallel."

    # We'll run the computation of the training, test and validation set in parallel, to reduce the overall time
    # We use the GNU parallel tool for this. On Ubuntu install the parallel tool via `sudo apt install parallel`.
    parallel --tag --line-buffer --no-notice GLOG_logtostderr=1 $TOOLS/convert_imageset_regression_flow \
        --gray \
        --shuffle \
        --encoded \
        --clip_value=$FLOW_CLIP_VALUE \
        --resize_width_flow=$RESIZE_FLOW \
        --resize_height_flow=$RESIZE_FLOW \
        $DATA_ROOT/ \
        $OUTPUT/{}.txt \
        $OUTPUT/ratsi_{}_lmdb ::: train val test
    echo "All done".

else
    echo "Create databased in sequential order."

    echo "Creating train lmdb..."
    GLOG_logtostderr=1 $TOOLS/convert_imageset_regression_flow \
	    --gray \
        --shuffle \
        --encoded \
        --clip_value=$FLOW_CLIP_VALUE \
        --resize_width_flow=$RESIZE_FLOW \
        --resize_height_flow=$RESIZE_FLOW \
        $DATA_ROOT/ \
        $OUTPUT/train.txt \
        $OUTPUT/ratsi_train_lmdb

    echo "Creating test lmdb..."
    GLOG_logtostderr=1 $TOOLS/convert_imageset_regression_flow \
	    --gray \
        --shuffle \
        --encoded \
	    --clip_value=$FLOW_CLIP_VALUE \
        --resize_width_flow=$RESIZE_FLOW \
        --resize_height_flow=$RESIZE_FLOW \
        $DATA_ROOT/ \
        $OUTPUT/test.txt \
        $OUTPUT/ratsi_test_lmdb

    echo "Creating val lmdb..."
    GLOG_logtostderr=1 $TOOLS/convert_imageset_regression_flow \
	    --gray \
        --shuffle \
        --encoded \
        --clip_value=$FLOW_CLIP_VALUE \
        --resize_width_flow=$RESIZE_FLOW \
        --resize_height_flow=$RESIZE_FLOW \
        $DATA_ROOT/ \
        $OUTPUT/val.txt \
        $OUTPUT/ratsi_val_lmdb
fi

echo "Done."

