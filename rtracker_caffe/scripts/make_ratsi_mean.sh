#!/usr/bin/env sh
# Compute the mean image from the ratsi training lmdb
# N.B. this is available in data

DATA=~/rbr_cnn/rtracker_caffe/data
TOOLS=build/tools

$TOOLS/compute_image_mean $DATA/ratsi_train_lmdb \
  $DATA/ratsi_mean.binaryproto

echo "Done."
