#!/usr/bin/env sh
# Create the ratsi lmdb inputs
set -e


DATA_ROOT=~/datasets/RatSI
OUTPUT=~/rbr_cnn/rtracker_caffe/data_tracking
TOOLS=build/tools

python ~/rbr_cnn/rtracker_caffe/scripts/make_ratsi_aux_data.py ${DATA_ROOT} ${OUTPUT} -m -d -t c --scale 576

#echo "Creating lmdbs..."
#GLOG_logtostderr=1 python convert_images_regression.py ${DATA_ROOT} ${OUTPUT} --gray --shuffle
#echo "Done."

echo "Creating train lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset_regression \
	--gray \
    --shuffle \
    --encoded \
    $DATA_ROOT/ \
    $OUTPUT/train.txt \
    $OUTPUT/ratsi_train_lmdb
    
echo "Creating test lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset_regression \
	--gray \
    --shuffle \
    --encoded \
    $DATA_ROOT/ \
    $OUTPUT/test.txt \
    $OUTPUT/ratsi_test_lmdb

echo "Creating val lmdb..."

GLOG_logtostderr=1 $TOOLS/convert_imageset_regression \
	--gray \
    --shuffle \
    --encoded \
    $DATA_ROOT/ \
    $OUTPUT/val.txt \
    $OUTPUT/ratsi_val_lmdb
    
echo "Done."
