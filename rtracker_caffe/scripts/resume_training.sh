#!/usr/bin/env sh
set -e

./build/tools/caffe train \
    --solver=/home/malte/rbr_cnn/rtracker_caffe/net/solver.prototxt \
    --snapshot=/home/malte/rbr_cnn/rtracker_caffe/models/caffe_alexnet_train_#####.solverstate.h5 \
    $@
