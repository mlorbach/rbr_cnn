import os
import errno
import numpy as np


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def label_merge_transform(Y, labels_to_merge, group_label=None):

    n_labels = len(labels_to_merge)

    if n_labels == 0:
        return Y

    if group_label is None:
        if n_labels > 0:
            group_label = labels_to_merge[0]
        else:
            group_label = 'merged'

    if n_labels == 2 and group_label == labels_to_merge[0]:
        Y[Y == labels_to_merge[1]] = group_label
    else:
        mask = np.in1d(Y, labels_to_merge)
        Y[mask] = group_label

    return Y
