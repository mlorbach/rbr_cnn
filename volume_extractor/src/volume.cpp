#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
//#include "opencv2/cudaarithm.hpp"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;


// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
std::vector<fs::path> get_image_files(const fs::path& root, const std::string& ext = ".jpg")
{
	std::vector<fs::path> ret;

    if(!fs::exists(root) || !fs::is_directory(root))
    	return ret;

    fs::recursive_directory_iterator it(root);
    fs::recursive_directory_iterator endit;

    while(it != endit)
    {
        if(fs::is_regular_file(*it) && it->path().extension() == ext)
        	ret.push_back(it->path());
        ++it;
    }

    std::sort(ret.begin(), ret.end());

    return ret;
}


std::vector<std::vector<cv::Point2f> > read_tracking_file(const fs::path& filename, const int skip_header, const float scale_w, const float scale_h)
{
	std::vector<std::vector<cv::Point2f> > data;

	std::ifstream in(filename.c_str());
	if (!in.is_open())
	{
		std::cerr << "[ERROR] Failed to open tracking file: " << filename << std::endl;
		return data;
	}

	char *endptr;
	int count = 0;
	int errors = 0;
	std::string current_line;

	while(getline(in, current_line))
	{
		if(count < skip_header){
			count++;
			continue;
		}

	   std::vector<cv::Point2f> values;
	   std::stringstream temp(current_line);
	   std::string pos;
	   cv::Point2f point;
	   try{
		   int i = 0;
		   while(getline(temp, pos, ';'))
		   {
			   if(i == 0)
			   {
				   ++i;
				   continue; // index
			   }

			   if(i % 2 == 1)
			   {
				   point.x = atoi(pos.c_str()) * scale_w;
			   }
			   else
			   {
				   point.y = atoi(pos.c_str()) * scale_h;
				   values.push_back(point);
			   }
			   i++;
		   }

		   if(values.size() != 6){
			   std::cout << "[WARN] Incomplete row (l: " << count << "). Skip." << std::endl;
			   count++;
			   errors++;
			   continue;
		   }
		   data.push_back(values);
		   count++;
	   }
	   catch(const std::bad_cast& exc){
		   std::cerr << "[WARN] Parse error (l: " << count << "): '" << pos << "'" << std::endl;// with " << exc.what() << std::endl;
		   count++;
		   errors++;
		   continue;
	   }
	}

	std::cout << "Parsed " << count << " lines and encountered " << errors << " errors." << std::endl;

	return data;
}


int main(int argc, char** argv )
{
	int limit_frames;
	fs::path input_path, output_path, input_tracking;
	bool verbose;

	/***************************************/
	// COMMAND LINE ARGUMENTS
	/***************************************/

	// Declare the supported options.
	po::options_description desc("Usage");
	desc.add_options()
		("help,h", "produce help message")
		("input_path,I", po::value<fs::path>(&input_path)->default_value("/home/malte/datasets/RatSI/video_frames/Observation01"),
				"Path to input images")
		("tracking_file,t", po::value<fs::path>(&input_tracking)->default_value("/home/malte/datasets/RatSI/tracking/Observation01.csv"),
				"Path to tracking file")
		("output_path,O", po::value<fs::path>(&output_path)->default_value("output"),
				"Output path")
		("limit,l", po::value<int>(&limit_frames)->default_value(-1), "Number of images to process (-1: all)")
		("verbose,v", po::value<bool>(&verbose)->default_value(false), "Verbose output, show images")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}


	// Load image list and tracking data
	// ---------------------------------
	std::vector<fs::path> image_list = get_image_files(input_path);
	std::vector<std::vector<cv::Point2f> > tracks = read_tracking_file(input_tracking, 3, 255/576., 255/576.);

	if(image_list.size() != tracks.size())
	{
		std::cout << "[WARN] Number of images doesn't match number of tracking points: (" << image_list.size() << " vs. " << tracks.size() << ")." << std::endl;
	}

	// Prepare output
	// --------------
	cv::RNG rng(cv::getTickCount());
	cv::Mat image, output;

	if(verbose)
	{
		cv::namedWindow("Input", cv::WINDOW_AUTOSIZE );
		cv::moveWindow("Input", 300, 200);
		cv::namedWindow("crop1", cv::WINDOW_AUTOSIZE );
		cv::moveWindow("crop1", 560, 200);
		cv::namedWindow("crop2", cv::WINDOW_AUTOSIZE );
		cv::moveWindow("crop2", 560, 300);
	}

	// output folders
	boost::format fileout((output_path / "subject_%d" / "frame_%08d.jpg").string());
	fs::create_directories(output_path / "subject_0");
	fs::create_directories(output_path / "subject_1");

	// Loop over frames
	// ----------------

	bool cancel = false;
	size_t total_frames = std::min(image_list.size(), tracks.size());
	char k;
	size_t frame;
	for(frame = 0; frame < total_frames && frame < limit_frames && !cancel; ++frame)
	{
//		size_t frame = rng.uniform(0, tracks.size());

		if(verbose)
			std::cout << "[INFO] Reading frame " << frame << ": " << image_list.at(frame) << std::endl;

		image = cv::imread(image_list.at(frame).string(), cv::IMREAD_GRAYSCALE);

		if ( !image.data )
		{
			std::cout << "[ERROR] Failed to read image." << std::endl;
			return -1;
		}

		// Determine relative orientation and distance
		const int n_pts = 3;
		cv::Point2f diff = tracks.at(frame).at(0) - tracks.at(frame).at(n_pts);
		float dist_center = cv::sqrt(diff.x*diff.x + diff.y*diff.y);
		float phi_relative = std::atan2(diff.y, diff.x)  * 180 / CV_PI;

		if(verbose)
			std::cout << "[DEBG] Relative orientation: " << phi_relative << " (deg), dist: " << dist_center << "px." << std::endl;

		// Draw tracking points
		// --------------------

		if(verbose)
		{
			// make image an color image (only for visualization)
			cv::cvtColor(image, output, cv::COLOR_GRAY2BGR);
		}

		// define drawing colors
		std::vector<cv::Scalar> colors;
		colors.push_back(cv::Scalar(0,0,255));
		colors.push_back(cv::Scalar(0,255,0));
		cv::Scalar comCol(255,0,0);

		// some drawing properties
		const int marker_size = 10;

		// box extraction
		const cv::Size2f box_size(75, 75);
		cv::Mat crop[2];
		const float angle_offset[] = {180, 0};

		// draw tracking points for all animals
		for(size_t i = 0; i < 2; ++i) // animals
		{
			// bbox
			cv::RotatedRect bbox(tracks.at(frame).at(i*n_pts), box_size, phi_relative);

			// extract rotated bounding box
			cv::Mat R, rotated;
			R = cv::getRotationMatrix2D(tracks.at(frame).at(i*n_pts), phi_relative + angle_offset[i], 1);
			cv::warpAffine(image, rotated, R, image.size(), cv::INTER_CUBIC);
			cv::getRectSubPix(rotated, box_size, bbox.center, crop[i]);

			if(verbose)
			{
				// draw bounding box
				cv::Point2f bbox_points[4]; bbox.points(bbox_points);
				for(int j = 0; j < 4; ++j)
					cv::line(output, bbox_points[j], bbox_points[(j+1)%4], colors.at(i), 1, 8);

				// center -> nose, with arrow on nose
				cv::arrowedLine(output, tracks.at(frame).at(i*n_pts), tracks.at(frame).at(i*n_pts+1), colors.at(i), 1, 8, 0, .3);

				// center -> tail
				cv::line(output, tracks.at(frame).at(i*n_pts), tracks.at(frame).at(i*n_pts+2), colors.at(i), 1, 8);

				// center point
				cv::drawMarker(output, tracks.at(frame).at(i*n_pts), colors.at(i), cv::MARKER_TILTED_CROSS, marker_size, 1, 8);
			}
		}

		if(verbose)
		{
			cv::line(output, tracks.at(frame).at(0), tracks.at(frame).at(n_pts), comCol, 2, 8);

			cv::imshow("Input", output);
			cv::imshow("crop1", crop[0]);
			cv::imshow("crop2", crop[1]);

			k = cv::waitKey(0);
			switch(k)
			{
			case 'q':
			case 27:
				cancel = true;
				break;
			}
		}
		else
		{
			cv::imwrite( (fileout % 0 % frame).str() , crop[0]);
			cv::imwrite( (fileout % 1 % frame).str() , crop[1]);
			if((frame+1) % 500 == 0)
				std::cout << "[INFO] Processed " << frame+1 << "/" << total_frames << std::endl;
		}
	}

	std::cout << "[INFO] Done. Processed " << frame << " frames." << std::endl;

    return 0;
}


