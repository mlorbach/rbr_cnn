#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>
#include <iterator>
#include <opencv2/opencv.hpp>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
//#include "opencv2/cudaarithm.hpp"

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

typedef std::deque<cv::Point2f> Queue;


std::vector<std::vector<cv::Point2f> > read_tracking_file(const fs::path& filename, const int skip_header, const float scale_w, const float scale_h)
{
	std::vector<std::vector<cv::Point2f> > data;

	std::ifstream in(filename.c_str());
	if (!in.is_open())
	{
		std::cerr << "[ERROR] Failed to open tracking file: " << filename << std::endl;
		return data;
	}

	char *endptr;
	int count = 0;
	int errors = 0;
	std::string current_line;

	while(getline(in, current_line))
	{
		if(count < skip_header){
			count++;
			continue;
		}

	   std::vector<cv::Point2f> values;
	   std::stringstream temp(current_line);
	   std::string pos;
	   cv::Point2f point;
	   try{
		   int i = 0;
		   while(getline(temp, pos, ';'))
		   {
			   if(i == 0)
			   {
				   ++i;
				   continue; // index
			   }

			   if(i % 2 == 1)
			   {
				   point.x = atoi(pos.c_str()) * scale_w;
			   }
			   else
			   {
				   point.y = atoi(pos.c_str()) * scale_h;
				   values.push_back(point);
			   }
			   i++;
		   }

		   if(values.size() != 6){
			   std::cout << "[WARN] Incomplete row (l: " << count << "). Skip." << std::endl;
			   count++;
			   errors++;
			   continue;
		   }
		   data.push_back(values);
		   count++;
	   }
	   catch(const std::bad_cast& exc){
		   std::cerr << "[WARN] Parse error (l: " << count << "): '" << pos << "'" << std::endl;// with " << exc.what() << std::endl;
		   count++;
		   errors++;
		   continue;
	   }
	}

	std::cout << "Parsed " << count << " lines and encountered " << errors << " errors." << std::endl;

	return data;
}


int main(int argc, char** argv )
{
	int limit_frames, output_w;
	fs::path input_path, output_path, input_tracking;
	bool verbose;

	/***************************************/
	// COMMAND LINE ARGUMENTS
	/***************************************/

	// Declare the supported options.
	po::options_description desc("Usage");
	desc.add_options()
		("help,h", "produce help message")
		("input_video,I", po::value<fs::path>(&input_path)->default_value("/home/malte/datasets/RatSI/videos/Observation01.mp4"),
				"Path to input video")
		("tracking_file,t", po::value<fs::path>(&input_tracking)->default_value("/home/malte/datasets/RatSI/tracking/Observation01.csv"),
				"Path to tracking file")
		("output_path,O", po::value<fs::path>(&output_path)->default_value("output"),
				"Output path")
		("size,s", po::value<int>(&output_w)->default_value(0), "Size of output image (resized to this size, W=H). If 0, output original crop size.")
		("limit,l", po::value<int>(&limit_frames)->default_value(-1), "Number of images to process (-1: all)")
		("verbose,v", po::value<bool>(&verbose)->default_value(false), "Verbose output, show images")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 1;
	}


	// Setup video reader and tracking data
	// ------------------------------------
	std::vector<std::vector<cv::Point2f> > tracks = read_tracking_file(input_tracking, 3, 1, 1);

	cv::VideoCapture reader(input_path.string());
	if (!reader.isOpened())
	{
		std::cerr << "[ERROR] Can't open input video file: " << input_path << std::endl;
		return -1;
	}

	cv::Size2f video_size(reader.get(CV_CAP_PROP_FRAME_WIDTH), reader.get(CV_CAP_PROP_FRAME_HEIGHT));

	// Prepare output
	// --------------
	cv::Mat image, output;

	if(verbose)
	{
		cv::namedWindow("Input", cv::WINDOW_AUTOSIZE );
		cv::moveWindow("Input", 300, 200);
		cv::namedWindow("crop1", cv::WINDOW_AUTOSIZE );
		cv::moveWindow("crop1", 900, 200);
		cv::namedWindow("crop2", cv::WINDOW_AUTOSIZE );
		cv::moveWindow("crop2", 900, 380);
	}

	// define drawing colors
	std::vector<cv::Scalar> colors;
	colors.push_back(cv::Scalar(0,0,255));
	colors.push_back(cv::Scalar(0,255,0));
	cv::Scalar comCol(255,0,0);

	// some drawing properties
	const int marker_size = 10;
	const int n_pts = 3;

	// box extraction
	const bool output_rotated = false;
	const cv::Size2f box_size(170, 170);
	if(output_w <= 0)
		output_w = int(std::floor(box_size.width));
	const cv::Size2i output_size(output_w, output_w);
	cv::Mat crop;
	cv::Mat resized[2];
	const float angle_offset[] = {180, 0};

	const int N_avg = 4;
	Queue center_pt[2];
	cv::Point2f center_pt_mean[2];

	cv::Mat sum_data(output_size, CV_32SC3, cv::Scalar::all(0));

	// output folders
	fs::path video_name = input_path.filename().replace_extension("");
	boost::format fileout((output_path / "subject_%d" / video_name / "frame_%08d.jpg").string());
	fs::create_directories(output_path / "subject_0" / video_name);
	fs::create_directories(output_path / "subject_1" / video_name);

	std::ofstream fh_track((output_path / input_path.filename().replace_extension("csv")).c_str(), std::ios::binary);
	if(!fh_track.is_open())
	{
		std::cerr << "[ERROR] Failed to open file for writing tracking features." << std::endl;
		return -1;
	}

	// Loop over frames
	// ----------------

	bool cancel = false;
	char k;
	size_t frame;
	const std::string sep(";");
	for(frame = 0; frame < tracks.size() && frame < limit_frames && !cancel && reader.isOpened(); ++frame)
	{
		reader >> image;

		if ( !image.data )
		{
			if(frame >= tracks.size() * .9)
				break; // end of the video, let's stop here

			// but don't accept missing frames somewhere in the middle of the video:
			std::cout << "[ERROR] Failed to read frame " << frame << std::endl;
			break;
		}

		// Determine relative orientation and distance
		// -------------------------------------------

		// keep track of the previous N center point positions for averaging
		if(frame >= N_avg)
		{
			center_pt[0].pop_front();
			center_pt[1].pop_front();
		}
		center_pt[0].push_back(tracks.at(frame).at(0));
		center_pt[1].push_back(tracks.at(frame).at(n_pts));

		// compute average center points over previous N frames
		center_pt_mean[0] = std::accumulate(center_pt[0].begin(), center_pt[0].end(), cv::Point2f(0.f, 0.f)) / float(center_pt[0].size());
		center_pt_mean[1] = std::accumulate(center_pt[1].begin(), center_pt[1].end(), cv::Point2f(0.f, 0.f)) / float(center_pt[1].size());

		cv::Point2f diff = center_pt_mean[0] - center_pt_mean[1];
		float dist_center = cv::sqrt(diff.x*diff.x + diff.y*diff.y) / video_size.width;
		float phi_relative = std::atan2(diff.y, diff.x);
		float v[] = {0.f, 0.f};
		if(center_pt[0].size() > 1)
		{
			diff = center_pt[0].end()[-1] - center_pt[0].end()[-2];
			v[0] = cv::sqrt(diff.x*diff.x + diff.y*diff.y) / (box_size.width / 2.0f);
			diff = center_pt[1].end()[-1] - center_pt[1].end()[-2];
			v[1] = cv::sqrt(diff.x*diff.x + diff.y*diff.y) / (box_size.width / 2.0f);
		}

		fh_track << frame << sep << dist_center << sep << phi_relative << sep << v[0] << sep << v[1] << std::endl;

		if(verbose)
			std::cout << "[DEBG] Relative orientation: " << phi_relative << " (rad), dist: " << dist_center << std::endl;

		// Draw tracking points
		// --------------------

		if(verbose)
		{
			// make image an color image (only for visualization)
//			cv::cvtColor(image, output, cv::COLOR_GRAY2BGR);
			image.copyTo(output);
		}


		// draw tracking points for all animals
		for(size_t i = 0; i < 2; ++i) // animals
		{
			// bbox extraction
			if(output_rotated)
			{
				cv::RotatedRect bbox(center_pt_mean[i], box_size, phi_relative * 180 / CV_PI);

				// extract rotated bounding box
				cv::Mat R, rotated;
				R = cv::getRotationMatrix2D(center_pt_mean[i], phi_relative * 180 / CV_PI + angle_offset[i], 1);
				cv::warpAffine(image, rotated, R, image.size(), cv::INTER_CUBIC);
				cv::getRectSubPix(rotated, box_size, bbox.center, crop);

				if(verbose)
				{
					// draw bounding box
					cv::Point2f bbox_points[4]; bbox.points(bbox_points);
					for(int j = 0; j < 4; ++j)
						cv::line(output, bbox_points[j], bbox_points[(j+1)%4], colors.at(i), 1, 8);
				}
			}
			else
			{
				cv::Rect2f bbox(center_pt_mean[i].x - box_size.width/2.,
								center_pt_mean[i].y - box_size.height/2.,
								box_size.width, box_size.height);
				cv::getRectSubPix(image, box_size, center_pt_mean[i], crop);

				if(verbose)
				{
					// draw bounding box
					cv::rectangle(output, bbox, colors.at(i), 1, 8);
				}
			}

			cv::resize(crop, resized[i], output_size, 0, 0, cv::INTER_AREA);
			cv::add(sum_data, resized[i], sum_data, cv::noArray(), CV_32SC3);

			if(verbose)
			{
				// center -> nose, with arrow on nose
				cv::arrowedLine(output, tracks.at(frame).at(i*n_pts), tracks.at(frame).at(i*n_pts+1), colors.at(i), 1, 8, 0, .3);

				// center -> tail
				cv::line(output, tracks.at(frame).at(i*n_pts), tracks.at(frame).at(i*n_pts+2), colors.at(i), 1, 8);

				// center point
				cv::drawMarker(output, tracks.at(frame).at(i*n_pts), colors.at(i), cv::MARKER_TILTED_CROSS, marker_size, 1, 8);
			}
		}

		if(verbose)
		{
			cv::line(output, center_pt_mean[0], center_pt_mean[1], comCol, 2, 8);

			cv::imshow("Input", output);
			cv::imshow("crop1", resized[0]);
			cv::imshow("crop2", resized[1]);

			k = cv::waitKey(0);
			switch(k)
			{
			case 'q':
			case 27:
				cancel = true;
				break;
			}
		}
		else
		{
			cv::imwrite( (fileout % 0 % frame).str() , resized[0]);
			cv::imwrite( (fileout % 1 % frame).str() , resized[1]);
			if((frame+1) % 500 == 0)
				std::cout << "[INFO] Processed " << frame+1 << "/" << tracks.size() << std::endl;
		}
	}

	fh_track.close();

	// -----------------------------
	// compute mean image and output
	// -----------------------------

	sum_data = sum_data / float(2 * frame);
	std::cout << "Mean intensity over all seen boxes: " << cv::mean(sum_data) << std::endl;

	std::string fn_mean = (output_path / input_path.filename().replace_extension("")).string() + "_mean.txt";
	std::ofstream fh_mean(fn_mean.c_str(), std::ios::binary);
	if(!fh_mean.is_open())
	{
		std::cerr << "[ERROR] Failed to open file for writing tracking features." << std::endl;
		return -1;
	}
	fh_mean << cv::mean(sum_data);
	fh_mean.close();

	sum_data.convertTo(sum_data, CV_8UC3, 1, 0);
	cv::imwrite(fs::path(fn_mean).replace_extension("jpg").c_str(), sum_data);

	// -----------------------------
	// -----------------------------

	std::cout << "[INFO] Done. Processed " << frame << " frames." << std::endl;

    return 0;
}


